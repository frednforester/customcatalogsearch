package mil.navsup.oim.ui.customcatalogsearch.model.eo;

public class RequestedUser {
    
    
    private String displayName;
    private String usrKey;
    private String usrId;
    private String email;
    private String usrDisplayName;
    private String userLogin;
    private String login;
    private String id;
    private String organizationName;
    private String altTokenIndicator;
    private String firstName;
    private String lastName;
    private String entityId;
    private String selectedNDPRole;
    private String selectedOrg;
    
    
    
    public RequestedUser() {
        super();
    }


    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setAltTokenIndicator(String altTokenIndicator) {
        this.altTokenIndicator = altTokenIndicator;
    }

    public String getAltTokenIndicator() {
        return altTokenIndicator;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setSelectedNDPRole(String selectedNDPRole) {
        this.selectedNDPRole = selectedNDPRole;
    }

    public String getSelectedNDPRole() {
        return selectedNDPRole;
    }

    public void setSelectedOrg(String selectedOrg) {
        this.selectedOrg = selectedOrg;
    }

    public String getSelectedOrg() {
        return selectedOrg;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setUsrKey(String usrKey) {
        this.usrKey = usrKey;
    }

    public String getUsrKey() {
        return usrKey;
    }

    public void setUsrId(String usrId) {
        this.usrId = usrId;
    }

    public String getUsrId() {
        return usrId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }


    public void setUsrDisplayName(String usrDisplayName) {
        this.usrDisplayName = usrDisplayName;
    }

    public String getUsrDisplayName() {
        return usrDisplayName;
    }

    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append(this.getUsrKey() + ",");
        buf.append(this.usrDisplayName + ",");
        buf.append(this.email + ",");
        buf.append(this.displayName + ",");
        buf.append(this.altTokenIndicator + ",");
        buf.append(this.getId());
        return buf.toString();
    }
}
