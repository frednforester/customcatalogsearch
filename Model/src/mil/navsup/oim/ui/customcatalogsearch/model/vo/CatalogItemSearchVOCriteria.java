package mil.navsup.oim.ui.customcatalogsearch.model.vo;

import java.util.HashMap;
import java.util.List;

import mil.navsup.oim.ui.customcatalogsearch.model.eo.RequestedCatalogItem;
import mil.navsup.oim.ui.customcatalogsearch.model.eo.RequestedUser;

public class CatalogItemSearchVOCriteria {
    
    
    private String entityType;
    private String entityDisplayName;
    private String applicationName;
    private String returnMessage;
    private List<RequestedCatalogItem> requestedItems;
    
    
    public CatalogItemSearchVOCriteria() {
        super();
    }


    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityDisplayName(String entityDisplayName) {
        this.entityDisplayName = entityDisplayName;
    }

    public String getEntityDisplayName() {
        return entityDisplayName;
    }


    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }

    public String getReturnMessage() {
        return returnMessage;
    }

   
    
    public void setRequestedItems(List<RequestedCatalogItem> requestedItems) {
        this.requestedItems = requestedItems;
    }

    public List<RequestedCatalogItem> getRequestedItems() {
        return requestedItems;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicationName() {
        return applicationName;
    }

}
