package mil.navsup.oim.ui.customcatalogsearch.model.vo;

public class StaticSQL {

    public static final Integer MAX_RECORDS_FROM_SEARCH = 25;

    public static final String APPICATIONDATASOURCE = "jdbc/OIMDS";

    public static final String FINDUSERS_USR_LOGIN = "UPPER(usr.usr_login) like UPPER(?) ";
        public static final String FINDUSERS_USR_EMAIL = "UPPER(usr.usr_email) like UPPER(?)";
        public static final String FINDUSERS_USR_DISPLAYNAME = "UPPER(usr.usr_display_name) like UPPER(?) ";
        public static final String FINDUSERS_USR_ORGNAME = "act.act_key = ? ";
        public static final String FINDUSERS_USR_ROLENAME = "ugp.ugp_key = ? ";
        
        
        public static final String FINDUSERS_USR_ROLEKEY = "usr.usr_key = usg.usr_key and usg.ugp_key = ugp.ugp_key  ";
        
        
        public static final String FINDUSERS_USR_ROLEFROM = ",usg,ugp";
        
        public static final String FINDUSERS = 
                                   "SELECT usr.usr_login,usr.usr_key,usr.usr_display_name,usr.usr_email,act.act_name,usr.usr_first_name,usr.usr_last_name," +
                                   "USR_UDF_ALTTOKENINDIC as ALTTOKENINDIC " +
                                   "from usr,act<ROLEFROM> " +
                                   "where " +
                       "usr.usr_status = 'Active' and " +
                                   "usr.act_key = act.act_key " +
                                   "<ROLEKEY> " +
                                   "<ROLENAME> " +
                                   "<LOGIN> " +           
                                   "<DISPLAYNAME> " +
                                   "<EMAIL> " + 
                                   "<ORGNAME> ";
        
        
        public static final String FINDCATALOGITEMS = 
                                     "select catalog_id,entity_type,entity_display_name,entity_name,entity_key from catalog where is_requestable=1 and is_deleted=0 ";
        
        public static final String FINDCATALOGITEMS__ENTITYT_TYPE =  " and entity_type = ?";
        public static final String FINDCATALOGITEMS_DISPLAYNAME = " and (UPPER(entity_display_name) LIKE UPPER(?) OR UPPER(entity_name) LIKE UPPER(?))";
        public static final String FINDCATALOGITEMS_USERTAGS_ROLE = " and UPPER(user_defined_tags) LIKE (?)";
        public static final String FINDCATALOGITEMS_USERTAGS_ROLES = " UPPER(user_defined_tags) LIKE (?)";
        public static final String FINDCATALOGITEMS_USERTAGS_ORG = " and UPPER(user_defined_tags) like UPPER(?)";
        public static final String FINDCATALOGITEMS_APPANDENTITLEMENT = 
                                                    " and parent_entity_key = (select app_instance_key from app_instance where upper(app_instance_name)=upper(?))";
            
    public static final String GETUSERSREWQUESTBYENTITY = 
                "select " +
                "rq.request_id, " +
                "rbe.RBE_BENEFICIARY_KEY, " +
                "rbe.RBE_ENTITY_TYPE, " +
                "rbe.RBE_ENTITY_NAME, " +
                "rbe.RBE_ENTITY_KEY, " +
                "rbe.RBE_OPERATION, " +
                "rq.request_status " +
                "from  " +
                "request rq,REQUEST_BENEFICIARY_ENTITIES rbe " +
                "where  " +
                "rq.request_key = rbe.RBE_REQUEST_KEY and " +
                "rbe.RBE_BENEFICIARY_KEY = ? and " +
                "rbe.RBE_ENTITY_KEY = ? and " +
                "rq.request_isparent = 'false' and " +
                "rq.request_end_date is null and " +
                "rq.request_status not like '%Completed%' and  " +
                "rq.request_status not like '%Failed%' and  " +
                "rq.request_status not like '%Draft%' and  " +
                "rq.request_status not like '%Closed%' and " +
                "rq.request_status not like '%Withdrawn%' and " +
                "rq.request_status not like '%Rejected%' and " +
                "rq.request_status not like '%Expired%' and " +
                "rq.request_status not like '%Approved%'";
                             
    
}
