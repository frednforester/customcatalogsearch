package mil.navsup.oim.ui.customcatalogsearch.model.vo;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.Map;

import java.util.Set;

import javax.naming.Context;

import javax.sql.DataSource;

import mil.navsup.oim.ui.customcatalogsearch.model.eo.RequestedCatalogItem;
import mil.navsup.oim.ui.customcatalogsearch.model.eo.RequestedUser;

import oracle.adf.share.logging.ADFLogger;


public class SearchCatalogItemWrapper {
    
    
    
    private ADFLogger logger = ADFLogger.createADFLogger(SearchCatalogItemWrapper.class);
    public SearchCatalogItemWrapper() {
        super();
    }
    
    public List<RequestedCatalogItem> findCatalogItems(CatalogItemSearchVOCriteria criteria) throws Exception  {
        
        ResultSet rs = null;
        PreparedStatement stmnt = null;
        Connection conn = null;
        
        List<RequestedCatalogItem> catItemsList = new ArrayList<RequestedCatalogItem>();
        if (criteria == null)
         return catItemsList;
        
        
        String entityType = criteria.getEntityType();
        if (entityType != null && entityType.trim().isEmpty())
                entityType = null;
        
        String entityDisplayName = criteria.getEntityDisplayName();
        if (entityDisplayName != null && entityDisplayName.trim().isEmpty())
                entityDisplayName = null;
        
        String applicationName = criteria.getApplicationName();
        if (applicationName != null && applicationName.trim().isEmpty())
                applicationName = null;
        
        String sql = StaticSQL.FINDCATALOGITEMS;
        Map<Integer,String> preparedMap = new HashMap<Integer,String>();
        int parmCounter = 0;
  
        if (entityType != null) {
            parmCounter++;
            sql = sql + StaticSQL.FINDCATALOGITEMS__ENTITYT_TYPE;
            preparedMap.put(parmCounter,entityType);                          
        }
        
        if (entityDisplayName != null) {
            parmCounter++;
            sql = sql + StaticSQL.FINDCATALOGITEMS_DISPLAYNAME;
            preparedMap.put(parmCounter,"%" + entityDisplayName + "%");          
            parmCounter++;
            preparedMap.put(parmCounter,"%" + entityDisplayName + "%");
        }
        
        
        
        if (entityType.equals("Entitlement"))
        {
            if (applicationName != null && applicationName.length() > 0) {
                parmCounter++;
                sql = sql + StaticSQL.FINDCATALOGITEMS_APPANDENTITLEMENT;
                preparedMap.put(parmCounter,applicationName);       
                
            }
                              
        }
        
        
        if (parmCounter == 0) {
            sql = sql.replace("where","");
        }
        
        try {
            conn = this.getDataSourceConnection(StaticSQL.APPICATIONDATASOURCE);
        } catch (Exception e) {
            logger.finest("Error getting datasource connection:" + e.getMessage());
        }
        
        int tblSize = StaticSQL.MAX_RECORDS_FROM_SEARCH;
        try {

                stmnt = conn.prepareStatement(sql);
                Set<Integer> keys = preparedMap.keySet();
                for(Integer key : keys) {
                        if(preparedMap.get(key) == null)
                                continue;
                        String parm = preparedMap.get(key);
                        //parm = parm + "%";
                        logger.finest("Setting:" + key + " to:" + parm);
                        stmnt.setString(key, parm);
                }
                
                rs = stmnt.executeQuery();
                logger.finest("running query:" + sql);
                int count = 0;
                while (rs.next()) {
                    count++;
                    if (count > tblSize) {
                            criteria.setReturnMessage("Too Many Records. Please refine your search");
                            break;
                    }
                    RequestedCatalogItem catItem = new RequestedCatalogItem();
                    
                    catItem.setEntityDisplayName(rs.getString("entity_display_name"));
                    catItem.setEntityName(rs.getString("entity_name"));
                    catItem.setEntityType(rs.getString("entity_type"));
                    catItem.setId(rs.getString("catalog_id"));
                    catItem.setEntityKey(rs.getString("entity_key"));
                    logger.finest("CatRec:" + catItem);
                    catItemsList.add(catItem);
                }
                logger.finest("done query num recs:" + catItemsList.size());
        } 
        catch (Exception e) 
        {
            criteria.setReturnMessage("ERROR:" + e.getMessage());
            logger.severe("Error getting catalog data:" + e.getMessage(),e);
        } 
        finally 
        {
            try {
                    if (rs != null)
                            rs.close();
                    if (stmnt != null)
                            stmnt.close();
                    if (conn != null)
                            conn.close();
            } catch (Exception e) {
                    
            }
        }
        
        return catItemsList;
        
        
    }
    
    
    private Connection getDataSourceConnection(String dsName) throws Exception {
        
        Connection connection = null;
        try {
          Context initialContext = new javax.naming.InitialContext();
          DataSource dataSource = (DataSource)initialContext.lookup(dsName);
          connection = dataSource.getConnection();
          return connection;
         } catch(Exception e){
              throw e;
              //or handle more gracefully 
         }
        
    }
    
   
    
}
