package mil.navsup.oim.ui.customcatalogsearch.model.vo;

public class UserSearchVOCriteria {
    
    
    private String displayName;
    private String userLogin;
    private String email;
    private String roleKey;
    private String orgKey;
    
    private Integer maxRecords = 0;
    private String returnMessage;
    
    public UserSearchVOCriteria() {
        super();
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setRoleKey(String roleKey) {
        this.roleKey = roleKey;
    }

    public String getRoleKey() {
        return roleKey;
    }

    public void setOrgKey(String orgKey) {
        this.orgKey = orgKey;
    }

    public String getOrgKey() {
        return orgKey;
    }


    public void setMaxRecords(Integer maxRecords) {
        this.maxRecords = maxRecords;
    }

    public Integer getMaxRecords() {
        return maxRecords;
    }

    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }

    public String getReturnMessage() {
        return returnMessage;
    }

}
