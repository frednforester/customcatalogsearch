package mil.navsup.oim.ui.customcatalogsearch.model.vo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.naming.Context;

import javax.sql.DataSource;

import mil.navsup.oim.ui.customcatalogsearch.model.eo.RequestedUser;

import oracle.adf.share.logging.ADFLogger;

public class SearchUserWrapper {
    
    private ADFLogger logger = ADFLogger.createADFLogger(SearchUserWrapper.class);
    
    
    public SearchUserWrapper() {
        super();
    }
    
    public List<RequestedUser> findUsers(UserSearchVOCriteria criteria) throws Exception  {
        
        
        logger.finest("findUsers");
        ResultSet rs = null;
        PreparedStatement stmnt = null;
        Connection conn = null;
        List<RequestedUser> usersList = new ArrayList<RequestedUser>();
        
        if (criteria == null)
         return usersList;
        
        String displayName = criteria.getDisplayName();
        if (displayName != null && displayName.trim().isEmpty())
                displayName = null;
        String login = criteria.getUserLogin();
        if (login != null && login.trim().isEmpty())
                login = null;
        String email = criteria.getEmail();
        if (email != null && email.trim().isEmpty())
                email = null;
        String roleName = criteria.getRoleKey();
        if (roleName != null && roleName.trim().isEmpty())
                roleName = null;
        String orgName = criteria.getOrgKey();
        if (orgName != null && orgName.trim().isEmpty())
                orgName = null;
        
        
        String sql = StaticSQL.FINDUSERS;
        Map<Integer,String> preparedMap = new HashMap<Integer,String>();
        int parmCounter = 0;
        if (roleName != null) {
                parmCounter++;
                sql = sql.replace("<ROLEFROM>",StaticSQL.FINDUSERS_USR_ROLEFROM);
                sql = sql.replace("<ROLEKEY>"," and " + StaticSQL.FINDUSERS_USR_ROLEKEY);
                sql = sql.replace("<ROLENAME>"," and " + StaticSQL.FINDUSERS_USR_ROLENAME);
                preparedMap.put(parmCounter,roleName);
                //NavyADFUtils.setSessionScopeProperty("selectedNBISRole",roleName);
        }
        else {
                sql = sql.replace("<ROLEFROM>","");
                sql = sql.replace("<ROLEKEY>","");
                sql = sql.replace("<ROLENAME>","");
                //NavyADFUtils.setSessionScopeProperty("selectedNBISRole","");
                
        }
        
        if (login != null) {
                parmCounter++;
                sql = sql.replace("<LOGIN>"," and " + StaticSQL.FINDUSERS_USR_LOGIN);
                if (login.contains("*"))
                    login = login.replace("*", "%");
                else
                    login = login + "%";
                preparedMap.put(parmCounter,login);
        }
        else {
                sql = sql.replace("<LOGIN>","");
        }
        
        if (displayName != null) {
                parmCounter++;
                sql = sql.replace("<DISPLAYNAME>"," and " + StaticSQL.FINDUSERS_USR_DISPLAYNAME);
                if (displayName.contains("*"))
                    displayName = displayName.replace("*", "%");
                else
                    displayName = displayName + "%";
                preparedMap.put(parmCounter,displayName);
        }
        else {
                sql = sql.replace("<DISPLAYNAME>","");
        }
        
        if (email != null) {
                parmCounter++;
                sql = sql.replace("<EMAIL>"," and " + StaticSQL.FINDUSERS_USR_EMAIL);
                if (email.contains("*"))
                    email = email.replace("*", "%");
                else
                    email = email + "%";
                preparedMap.put(parmCounter,email);
        }
        else {
                sql = sql.replace("<EMAIL>","");
        }
        
        if (orgName != null) {
                parmCounter++;
                sql = sql.replace("<ORGNAME>"," and " + StaticSQL.FINDUSERS_USR_ORGNAME);
                preparedMap.put(parmCounter,orgName);
        }
        else {
                sql = sql.replace("<ORGNAME>","");
        }
        
        logger.finest("SQL:" + sql);
        logger.finest("Parms:" + preparedMap);
        
        try {
            conn = this.getDataSourceConnection(StaticSQL.APPICATIONDATASOURCE);
        } catch (Exception e) {
            logger.finest("Error getting datasource connection:" + e.getMessage());
        }
        
        int tblSize = StaticSQL.MAX_RECORDS_FROM_SEARCH;
        try {

                
                stmnt = conn.prepareStatement(sql);
                Set<Integer> keys = preparedMap.keySet();
                for(Integer key : keys) {
                        if(preparedMap.get(key) == null)
                                continue;
                        String parm = preparedMap.get(key);
                        //parm = parm + "%";
                        logger.finest("Setting:" + key + " to:" + parm);
                        stmnt.setString(key, parm);
                }
                
                rs = stmnt.executeQuery();
                logger.finest("running query");
                int count = 0;
                while (rs.next()) {
                    count++;
                    if (count > tblSize) {
                        criteria.setReturnMessage("Too Many Records. Please refine your search");
                        break;
                    }
                    RequestedUser user = new RequestedUser();
                    user.setDisplayName(rs.getString("usr_display_name"));
                    user.setUsrDisplayName(rs.getString("usr_display_name"));
                    user.setUserLogin(rs.getString("usr_login"));
                    user.setLogin(user.getUserLogin());
                    user.setId(rs.getString("usr_key"));
                    user.setUsrKey(rs.getString("usr_key"));
                    user.setEmail(rs.getString("usr_email"));
                    user.setOrganizationName(rs.getString("act_name"));
                    user.setAltTokenIndicator(rs.getString("ALTTOKENINDIC"));
                    user.setFirstName(rs.getString("usr_first_name"));
                    user.setLastName(rs.getString("usr_last_name"));
                    user.setEntityId(user.getId());
                    user.setSelectedNDPRole(roleName);
                    user.setSelectedOrg(orgName);
                    logger.finest("QRYRec:" + user);
                    
                    usersList.add(user);
                }
                logger.finest("done query num recs:" + usersList.size());
        } catch (Exception e) 
        {
            criteria.setReturnMessage("ERROR:" + e.getMessage());
            logger.severe("Error getting users:" + e.getMessage(),e);
        } 
        finally 
        {
            try {
                    if (rs != null)
                            rs.close();
                    if (stmnt != null)
                            stmnt.close();
                    if (conn != null)
                            conn.close();
            } catch (Exception e) {
                    
            }
        }
               
        return usersList;
                            
    }
    
    private Connection getDataSourceConnection(String dsName) throws Exception {
        
        Connection connection = null;
        try {
          Context initialContext = new javax.naming.InitialContext();
          DataSource dataSource = (DataSource)initialContext.lookup(dsName);
          connection = dataSource.getConnection();
          return connection;
         } catch(Exception e){
              throw e;
              //or handle more gracefully 
         }
        
    }
    
}
