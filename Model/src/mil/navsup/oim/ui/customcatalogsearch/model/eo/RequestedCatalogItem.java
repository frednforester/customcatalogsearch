package mil.navsup.oim.ui.customcatalogsearch.model.eo;

public class RequestedCatalogItem {
    
    private String entityName;
    private String entityDisplayName;
    private String entityType;
    private String entityKey;
    private String id;
    
    public RequestedCatalogItem() {
        super();
    }


    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityKey(String entityKey) {
        this.entityKey = entityKey;
    }

    public String getEntityKey() {
        return entityKey;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }


    public void setEntityDisplayName(String entityDisplayName) {
        this.entityDisplayName = entityDisplayName;
    }

    public String getEntityDisplayName() {
        return entityDisplayName;
    }

   
    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append("RequestedCatalogItem:[");
        buf.append("entityName:").append(entityName);
        buf.append("],");
        buf.append("[");
        buf.append("entityDisplayName:").append(entityDisplayName);
        buf.append("],");
        buf.append("[");
        buf.append("entityType:").append(entityType);
        buf.append("],");
        buf.append("[");
        buf.append("entityKey:").append(entityKey);
        buf.append("],");
        buf.append("[");
        buf.append("id:").append(id);
        buf.append("],");
        return buf.toString();
        
    }
}
