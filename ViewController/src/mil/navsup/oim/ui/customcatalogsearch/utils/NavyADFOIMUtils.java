package mil.navsup.oim.ui.customcatalogsearch.utils;


import java.util.HashSet;
import java.util.Set;

import com.thortech.util.logging.Logger;

import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.ui.platform.model.common.OIMClientFactory;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.identity.exception.NoSuchUserException;
import oracle.iam.identity.orgmgmt.api.OrganizationManager;
import oracle.iam.identity.orgmgmt.vo.Organization;
import oracle.iam.identity.orgmgmt.api.OrganizationManagerConstants.AttributeName;

import com.thortech.util.logging.Logger;

import oracle.iam.identity.rolemgmt.api.RoleManager;
import oracle.iam.identity.rolemgmt.vo.Role;

import java.util.List;

import oracle.iam.identity.rolemgmt.api.RoleManagerConstants;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import oracle.iam.provisioning.api.EntitlementService;
import oracle.iam.requestprofile.vo.RequestProfileConstants;
import oracle.iam.request.api.RequestService;
import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcColumnNotFoundException;
import Thor.API.Exceptions.tcInvalidLookupException;
import oracle.iam.provisioning.api.ApplicationInstanceService;
import oracle.iam.request.vo.Request;
import oracle.iam.request.vo.Beneficiary;
import oracle.iam.request.vo.RequestBeneficiaryEntity;
import oracle.iam.catalog.api.CatalogService;
import oracle.iam.platform.utils.vo.OIMType;
import oracle.iam.catalog.vo.Catalog;
import oracle.iam.provisioning.vo.Entitlement;
import oracle.iam.provisioning.vo.ApplicationInstance;
import oracle.iam.provisioning.exception.EntitlementNotFoundException;
import oracle.iam.provisioning.exception.ApplicationInstanceNotFoundException;
import Thor.API.tcResultSet;
import Thor.API.Operations.tcLookupOperationsIntf;

import oracle.iam.identity.usermgmt.api.UserManagerConstants;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import oracle.iam.api.OIMService;
import oracle.iam.identity.exception.UserMembershipException;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import oracle.iam.provisioning.api.ProvisioningService;
import oracle.iam.provisioning.exception.GenericProvisioningException;
import oracle.iam.provisioning.exception.UserNotFoundException;
import oracle.iam.provisioning.vo.Account;
import oracle.iam.provisioning.vo.EntitlementInstance;
import oracle.iam.ui.platform.utils.TaskFlowUtils;
import oracle.iam.request.api.RequestService;
@SuppressWarnings({"rawtypes","deprecation"})
public class NavyADFOIMUtils
{

    //private UserManager userOp;
    //private OrganizationManager orgOp;
    //private RoleManager roleOp;
    //private EntitlementService entOp;
    //private ApplicationInstanceService aiOp;
    //private RequestService reqOp;
    //private CatalogService catsvc;
    //private tcLookupOperationsIntf lookupOps;
    private static Logger logger = Logger.getLogger(NavyADFOIMUtils.class.getName());
    //private String[] operRoles = { "VIEWER", "CUSTODIAN", "AUTHOR", "ADHOC" };
    private static final HashMap<String,String> navsupUIMap = new HashMap<String,String>();
    SearchCriteria crit1 = new SearchCriteria(RoleManagerConstants.RoleAttributeName.NAME.getId(), "NBISPDA_", SearchCriteria.Operator.CONTAINS);
    SearchCriteria crit2 = new SearchCriteria(RoleManagerConstants.RoleAttributeName.NAME.getId(), "SYSTEM ADMINISTRATORS", SearchCriteria.Operator.EQUAL);
    SearchCriteria criteria = new SearchCriteria(crit1,crit2,SearchCriteria.Operator.OR);
    
    public NavyADFOIMUtils()
    {
        super();
        //userOp = OIMClientFactory.getUserManager();
        //orgOp = OIMClientFactory.getOrganizationManager();
        //roleOp = OIMClientFactory.getRoleManager();
        //entOp = OIMClientFactory.getEntitlementService();
        //aiOp = OIMClientFactory.getAppInstanceService();
        //reqOp = OIMClientFactory.getRequestService();
        //catsvc = OIMClientFactory.getCatalogService();
        
    }
    
    public ApplicationInstanceService getAiOp() {
        return OIMClientFactory.getAppInstanceService();
    }

    public RequestService getReqOp() {
        return OIMClientFactory.getRequestService();
    }

    public CatalogService getCatsvc() {
        return OIMClientFactory.getCatalogService();
    }

    public tcLookupOperationsIntf getLookupOp() {
        return OIMClientFactory.getLookupOperations();
    }
    
    public UserManager getUserOp() {
        return OIMClientFactory.getUserManager();
    }
    
    public OrganizationManager getOrgOp() {
        return OIMClientFactory.getOrganizationManager();
    }
    
    public RoleManager getRoleOp() {
        return OIMClientFactory.getRoleManager();
    }
    
    public EntitlementService getEntOp() {
        return OIMClientFactory.getEntitlementService();
        
    }
    
    public OIMService getOIMService() {
        return OIMClientFactory.getOIMService();
        
    }
    
    public RequestService getRequestOp() {
        return OIMClientFactory.getRequestService();
        
    }
    
    
    
     

    /**
     * Get User By Login ID
     *
     * returns null of user not found
     * @param userLogin
     
     */
    public User getUser(String userLogin, Set<String> retAttrs) throws Exception
    {
        logger.debug("Get User By Login:" + userLogin);
        // below demonstrates howto use the attributes
        // if we pass null tho we get them all back
        //Set<String> retAttrs = new HashSet<String>();
        //retAttrs.add(AttributeName.USER_KEY.getId());
        //retAttrs.add(AttributeName.USER_LOGIN.getId());
        //retAttrs.add(AttributeName.USERTYPE.getId());
        //retAttrs.add(AttributeName.EMAIL.getId());
        //retAttrs.add(AttributeName.FIRSTNAME.getId());
        //retAttrs.add(AttributeName.LASTNAME.getId());
        //retAttrs.add(AttributeName.STATUS.getId());

        User user = null;
        UserManager userOp = this.getUserOp();
        try
            {
                user = userOp.getDetails(userLogin, retAttrs, true);

            } catch (NoSuchUserException ex)
            {
                logger.debug("User Not Found!");
            } catch (Exception ex)
            {
                throw new Exception(ex);
            }
        return user;

    }

    public User getUser(Long userKey, Set<String> retAttrs) throws Exception
    {
        logger.debug("Get User By Key:" + userKey);
        // below demonstrates howto use the attributes
        // if we pass null tho we get them all back
        //Set<String> retAttrs = new HashSet<String>();
        //retAttrs.add(AttributeName.USER_KEY.getId());
        //retAttrs.add(AttributeName.USER_LOGIN.getId());
        //retAttrs.add(AttributeName.USERTYPE.getId());
        //retAttrs.add(AttributeName.EMAIL.getId());
        //retAttrs.add(AttributeName.FIRSTNAME.getId());
        //retAttrs.add(AttributeName.LASTNAME.getId());
        //retAttrs.add(AttributeName.STATUS.getId());

        User user = null;
        UserManager userOp = this.getUserOp();
        try
            {
                user = userOp.getDetails(userKey.toString(), retAttrs, false);

            } catch (NoSuchUserException ex)
            {
                logger.debug("User Not Found!");
            } catch (Exception ex)
            {
                throw new Exception(ex);
            }
        return user;

    }
    
    public String getUserKeyByLogin(String userLogin) throws Exception {
        Set<String> returnedAttributes = new HashSet<String>();
        returnedAttributes.add(UserManagerConstants.AttributeName.USER_KEY.getId());
        User u;
        try {
            u = this.getUser(userLogin, returnedAttributes);
        } catch (Exception e) {
            logger.error("API Error:" + e.getMessage(),e);
            throw e;
        }
        return u.getId();
    }

    public String getOrgName(String orgKey)
    {

        String orgName = null;
        Set<String> retAttrs = new HashSet<String>();
        retAttrs.add(AttributeName.ORG_NAME.getId());
        OrganizationManager orgOp = this.getOrgOp();
        try
            {
                Organization org = orgOp.getDetails(orgKey, retAttrs, false);
                orgName = (String)org.getAttribute(AttributeName.ORG_NAME.getId());

            } catch (Exception e)
            {
                logger.error("APIError:" + e.getMessage(), e);
            }
        return orgName;

    }

    /**
     * Get all operational Roles Associated with this user
     *
     
     * @exception Exception
     */
    public Map<String,String> getAllUsersOperRoles(String usrKey) throws Exception
    {
        
        Map<String,String> userRoles = new HashMap<String,String>();
        Set<String> retAttrs = new HashSet<String>();
        /*
        retAttrs.add(RoleManagerConstants.RoleAttributeName.CATEGORY_KEY.getId());
        retAttrs.add(RoleManagerConstants.RoleAttributeName.CREATE_DATE.getId());
        retAttrs.add(RoleManagerConstants.RoleAttributeName.DATA_LEVEL.getId());
        retAttrs.add(RoleManagerConstants.RoleAttributeName.DESCRIPTION.getId());
        retAttrs.add(RoleManagerConstants.RoleAttributeName.DISPLAY_NAME.getId());
        retAttrs.add(RoleManagerConstants.RoleAttributeName.EMAIL.getId());
        retAttrs.add(RoleManagerConstants.RoleAttributeName.KEY.getId());
        retAttrs.add(RoleManagerConstants.RoleAttributeName.LDAP_DN.getId());
        retAttrs.add(RoleManagerConstants.RoleAttributeName.LDAP_GUID.getId());
        retAttrs.add(RoleManagerConstants.RoleAttributeName.NAME.getId());
        retAttrs.add(RoleManagerConstants.RoleAttributeName.NAMESPACE.getId());
        retAttrs.add(RoleManagerConstants.RoleAttributeName.OWNER_KEY.getId());
        retAttrs.add(RoleManagerConstants.RoleAttributeName.UPDATE_DATE.getId());
        retAttrs.add(RoleManagerConstants.RoleAttributeName.UPDATED_BY.getId());
        */
        retAttrs.add(RoleManagerConstants.RoleAttributeName.UNIQUE_NAME.getId());

        Map configParms = new HashMap();
        configParms.put(RequestProfileConstants.SEARCH_STARTROW, "0");
        configParms.put(RequestProfileConstants.SEARCH_ENDROW, new Integer(Integer.MAX_VALUE).toString());
        RoleManager roleOp = this.getRoleOp();
        try
            {
                List<Role> roleList = roleOp.getUserMemberships(usrKey, criteria, retAttrs, configParms, false);
                for (Role r : roleList)
                    {  
                        String name = r.getUniqueName();
                        userRoles.put(r.getEntityId(),name);
                    }
            } catch (Exception ex)
            {
                logger.error("Exception:" + ex.getMessage(), ex);
                throw new Exception(ex);
            }

        return userRoles;

    }


    public Request getRequest(String requestId) throws Exception
    {
        RequestService reqOp = this.getReqOp();
        Request req = null;
        try
            {
                req = reqOp.getBasicRequestData(requestId);
            } catch (Exception ex)
            {
                logger.error("Exception:" + ex.getMessage(), ex);
                throw new Exception(ex);
            }
        return req;
    }


    public boolean isSAARRequired(Request request) throws Exception
    {
        logger.debug("isSAARRequired");
        try
            {
                List<Beneficiary> beneficiaries = request.getBeneficiaries();
                logger.debug("beneficiaries:" + beneficiaries.size());
                for (Beneficiary beneficiary : beneficiaries)
                    {
                        String tmp = beneficiary.getBeneficiaryKey();
                        Long userKey = new Long(tmp);
                        logger.debug("beneficiary:" + userKey);
                        User user1 = this.getUser(userKey, null);
                        logger.debug("Got beneficiary:" + user1.getLogin());
                        String orgStatus = (String)user1.getAttribute("ORGSTATUS");
                        logger.debug("orgStatus:" + orgStatus);
                        if ("Pending".equalsIgnoreCase(orgStatus))
                            {
                                if (this.isPdaRoleRequest(beneficiary))
                                    return true;
                            }
                    }
            } catch (Exception ex)
            {
                logger.error("Exception:" + ex.getMessage(), ex);
                throw new Exception(ex);
            }
        return false;
    }

    public boolean isPdaRoleRequest(Beneficiary beneficiary) throws Exception
    {
        logger.debug("isPdaRoleRequest");
        List<RequestBeneficiaryEntity> rbes = beneficiary.getTargetEntities();
        logger.debug("rbes:" + rbes.size());
        RoleManager roleOp = this.getRoleOp();
        for (RequestBeneficiaryEntity rbe : rbes)
            {
                String type = rbe.getEntityType();
                String key = rbe.getEntityKey();
                logger.debug("type:" + type);
                logger.debug("key:" + key);
                if (type.equalsIgnoreCase("Role"))
                    {
                        Role role = roleOp.getDetails(key, null);
                        logger.debug("Rolename:" + role.getName());
                        if (role.getName().toUpperCase().startsWith("NBISPDA"))
                            {
                                return true;
                            }
                    }
            }
        return false;
    }

    public HashSet<String> getRoleApprovers(Request request) throws Exception
    {
        HashSet<String> approvers = new HashSet<String>();
        List<Beneficiary> beneficiaries = request.getBeneficiaries();
        UserManager userOp = this.getUserOp();
        CatalogService catsvc = this.getCatsvc();
        RoleManager roleOp = this.getRoleOp();
        for (Beneficiary benf : beneficiaries)
        {
            List<RequestBeneficiaryEntity> rbes = benf.getTargetEntities();
            for (RequestBeneficiaryEntity rbe : rbes)
            {
                String key = rbe.getEntityKey();
                String type = rbe.getEntityType();
                logger.debug("Beneficiary Entity Type " + type);
                if (type.equalsIgnoreCase("Role"))
                {
                    Role role = roleOp.getDetails(key, null); 
                    logger.debug("roleAttrs=" + role.getAttributes());                
                    Catalog catitem = catsvc.getCatalogItemDetails(null, key, OIMType.Role, null);
                    String aprRole = catitem.getApproverRole();
                    String aprUser = catitem.getApproverUser();                   
                    if (aprRole != null && aprRole.trim().length() > 0)
                    {
                        List<User> members = roleOp.getRoleMembers(aprRole, false);
                        for(User member : members)
                        {
                            approvers.add(member.getLogin());
                        }       
                    }
                    if (aprUser != null && aprRole.trim().length() > 0)
                    {
                        User u = userOp.getDetails(aprUser, null, false);
                        if (u != null)
                        {
                            String userId = u.getLogin();
                            approvers.add(userId);
                        }
                    }
                    logger.debug("Approver Role:" + catitem.getApproverRole());
                    logger.debug("Approver User:" + catitem.getApproverUser());
                    logger.debug("All Approvers:" + approvers);
                }

            }
        }
        return approvers;
    }
    
    public HashSet<String> getBeneficiaries(Request request) throws Exception
    {
        HashSet<String> beneficiaryIds = new HashSet<String>();
        List<Beneficiary> beneficiaries = request.getBeneficiaries();
        UserManager userOp = this.getUserOp();
        for (Beneficiary benf : beneficiaries)
        {
            String userKey = benf.getBeneficiaryKey();
            User u = userOp.getDetails(userKey, null, false);
            if (u != null)
            {
                String userId = u.getLogin();
                beneficiaryIds.add(userId);
            }
        }
        
        return beneficiaryIds;
    }
    
    public String getRequester(Request request) throws Exception
    {
        
        String userLogin = null;
        String userKey = request.getRequesterKey();
        UserManager userOp = this.getUserOp();
        User u = userOp.getDetails(userKey, null, false);
        if (u != null)
        {
            userLogin = u.getLogin();
        }
        return userLogin;
    }
    
    public boolean isEntitlementPDA(String entityId) throws Exception
    {
        EntitlementService entOp = this.getEntOp();
        try
        {
            Entitlement ent = entOp.findEntitlement(new Long(entityId));
            if (ent != null)
            {
                ApplicationInstance ai = ent.getAppInstance();
                if (ai != null)
                {
                    String aiName = ai.getApplicationInstanceName();
                    if (aiName.toLowerCase().contains("pda"))
                        return true;
                }
            }
        }
        catch(EntitlementNotFoundException e)
        {
            return false;
        }
        catch(Exception e)
        {
            logger.error("Exception:" + e.getMessage(), e);
            throw new Exception(e);
        }
        return false;
    }
    
    public String getApplicationForEntitlement(String entityId) throws Exception
    {
        EntitlementService entOp = this.getEntOp();
        try
        {
            Entitlement ent = entOp.findEntitlement(new Long(entityId));
            if (ent != null)
            {
                ApplicationInstance ai = ent.getAppInstance();
                if (ai != null)
                {
                    return ai.getApplicationInstanceName();
                }
            }
        }
        catch(EntitlementNotFoundException e)
        {
            return null;
        }
        catch(Exception e)
        {
            logger.error("Exception:" + e.getMessage(), e);
            throw new Exception(e);
        }
        return null;
    }
    
    public ApplicationInstance getApplicationKeyForEntitlement(String entityId) throws Exception
    {
        EntitlementService entOp = this.getEntOp();
        try
        {
            Entitlement ent = entOp.findEntitlement(new Long(entityId));
            if (ent != null)
            {
                ApplicationInstance ai = ent.getAppInstance();
                if (ai != null)
                {
                    return ai;
                }
            }
        }
        catch(EntitlementNotFoundException e)
        {
            return null;
        }
        catch(Exception e)
        {
            logger.error("Exception:" + e.getMessage(), e);
            throw new Exception(e);
        }
        return null;
    }
    
    public Entitlement getEntitlementByKey(String entityId) throws Exception
    {
        EntitlementService entOp = this.getEntOp();
        try
        {
            Entitlement ent = entOp.findEntitlement(new Long(entityId));
            if (ent != null)
            {
               return ent;
            }
        }
        catch(EntitlementNotFoundException e)
        {
            return null;
        }
        catch(Exception e)
        {
            logger.error("Exception:" + e.getMessage(), e);
            throw new Exception(e);
        }
        return null;
    }
        
    public boolean isAppInstancePDA(String entityId) throws Exception
    {
        ApplicationInstanceService aiOp = this.getAiOp();
        try
        {
            ApplicationInstance ai = aiOp.findApplicationInstanceByKey(new Long(entityId));
            if (ai != null)
            {
                String aiName = ai.getApplicationInstanceName();
                if (aiName.toLowerCase().contains("pda"))
                    return true;
            }
        }
        catch(ApplicationInstanceNotFoundException e)
        {
            return false;
        }
        catch(Exception e)
        {
            logger.error("Exception:" + e.getMessage(), e);
            throw new Exception(e);
        }
        return false;
    }
    
    public void loadUILookup(String lookupName) 
    {
        logger.debug("OIMHelper started");
        tcLookupOperationsIntf lookupOps = this.getLookupOp();
        try {
            if (navsupUIMap.isEmpty()) {
                logger.debug("OIMHelper got lookupOps");
                tcResultSet resultSet = lookupOps.getLookupValues(lookupName);
                int amRow = resultSet.getRowCount();
                if (amRow == 0) {
                    logger.error("Lookup Code " + lookupName + " not found");
                }

                for (int i = 0; i < resultSet.getRowCount(); i++) {
                    resultSet.goToRow(i);
                    String codeKeyfromResultSet = resultSet.getStringValue("Lookup Definition.Lookup Code Information.Code Key");
                    String decodeValue = resultSet.getStringValue("Lookup Definition.Lookup Code Information.Decode");
                    navsupUIMap.put(codeKeyfromResultSet, decodeValue);
                }
                logger.debug("Navsup Lookup Loaded");
            }
        } catch (tcAPIException e) {
            logger.error("tcAPIException ", e);
        } catch (tcInvalidLookupException e) {
            logger.error("tcInvalidLookupException ", e);
        } catch (tcColumnNotFoundException e) {
            logger.error("tcColumnNotFoundException ", e);
        }
    }
    
    public String getUsersOrgName(String userKey) throws Exception
    {
        Set<String> retAttrs = new HashSet<String>();
        retAttrs.add(UserManagerConstants.AttributeName.USER_ORGANIZATION.getId());
        String orgName = null;
        
        try {
            User u = this.getUser(Long.valueOf(userKey), retAttrs);
            String orgKeystr = u.getOrganizationKey();
            orgName = this.getOrgName(orgKeystr);
        }
        catch(Exception e) {
            logger.error("APIError:" + e.getMessage(),e);
            throw e;
        }
        return orgName;
        
    }
    
    public Map<String,String> getEntitlementMetaData(Map<String,String> entitlements) {
        
        Map<String,String> entMap = new HashMap<String,String>();
        Set<String> keys = entitlements.keySet();
        CatalogService catsvc = this.getCatsvc();
        try {
            for(String entKey : keys) {
                Catalog item = catsvc.getCatalogItemDetails(null,entKey,OIMType.Entitlement,null);
                entMap.put(item.getEntityName(),item.getUserDefinedTags());
            }
        }
        catch(Exception e) {
            logger.error("Catalog Servicve Error:" + e.getMessage(),e);
            
        }
        return entMap;
        
    }
    public String getLookupProperty(String key) 
    {
        String property = null;
        property = navsupUIMap.get(key);
        logger.debug("Property:" + property);
        return property;

    }
    
    public DataSource getCustomDS(String dsName) throws Exception
    {
        DataSource customDS = null;
        try
        {
            InitialContext initialContext = new InitialContext();
            customDS = (DataSource)initialContext.lookup(dsName);
        }
        catch(Exception e)
        {
            throw e;
        }
        return customDS;
    }
    
    private boolean isApplicationProvisionedToUser(String poBenId,String aiName,List<String> validAccountStatus) {
        
        try {
            ProvisioningService provServ = OIMClientFactory.getProvisioningService();
            //accounts
            List<Account> accList = provServ.getAccountsProvisionedToUser(poBenId);
            List accKeyList = new ArrayList();
            for (Account accInst : accList) {
                String status = accInst.getAccountStatus();
                logger.debug("Account Status:::" + status);
                if (status != null && validAccountStatus.contains(status))
                    if (accInst.getAppInstance().getApplicationInstanceName().equalsIgnoreCase(aiName))
                        return true;
                    
            }
        }
        catch(Exception e) {
            logger.error("error getting benefciary provisioned accounts ", e);
        }
        return false;
    }
    
    
    public Map getUserProvisionedItems(String poBenId,List<String> validAccountStatus) {
        logger.debug("getUserProvisionedItems");
        logger.debug("benenficiary id:" + poBenId);
        Map<String,List<String>> ret = new HashMap<String,List<String>>();
        try {

            ProvisioningService provServ = OIMClientFactory.getProvisioningService();
            RoleManager roleMge = OIMClientFactory.getRoleManager();

            //roles
            List<Role> roleList = roleMge.getUserMemberships(poBenId, true);
            List<String> roleKeyList = new ArrayList<String>();

            for (Role role : roleList) {
                roleKeyList.add(role.getEntityId());
            }
            ret.put(OIMType.Role.getValue(), roleKeyList);
            logger.debug("roles resolved:" + roleKeyList.size());

            //entitlements
            List<EntitlementInstance> entList = provServ.getEntitlementsForUser(poBenId);
            List<String> entKeyList = new ArrayList<String>();
            for (EntitlementInstance inst : entList) {
                String status = inst.getStatus();
                logger.debug("Entitlement Status::::");
                if (status != null &&
                    (status.equals("Provisioned") || status.equals("Enabled")))
                    entKeyList.add(Long.toString(inst.getEntitlement().getEntitlementKey()));
            }
            ret.put(OIMType.Entitlement.getValue(), entKeyList);
            logger.debug("entitlements resolved:" + entList.size());

            //accounts
            List<Account> accList = provServ.getAccountsProvisionedToUser(poBenId);
            List<String> accKeyList = new ArrayList<String>();
            for (Account accInst : accList) {
                String status = accInst.getAccountStatus();
                logger.debug("Account Status:::" + status);
                if (status != null && validAccountStatus.contains(status))
                    accKeyList.add(accInst.getAppInstance().getApplicationInstanceKey() + "");
            }
            ret.put(OIMType.ApplicationInstance.getValue(), accKeyList);
            logger.debug("accounts resolved:" + accKeyList.size());

        } catch (UserNotFoundException e) {
            logger.error("error getting benefciary provisioned objects", e);
        } catch (GenericProvisioningException e) {
            logger.error("error getting benefciary provisioned objects ", e);
        } catch (AccessDeniedException e) {
            logger.error("error getting benefciary provisioned objects ", e);
        } catch (UserMembershipException e) {
            logger.error("error getting benefciary provisioned objects ", e);
        }

        logger.debug("getUserProvisionedItems");
        return ret;
    }
    
    public static void raiseTaskFlowLaunchEvent(String id, String taskFlowId, String name, String icon, String description, String helpTopicId, boolean inDialog, Map<String, Object> params)
    {
       String jsonPayLoad = TaskFlowUtils.createContextualEventPayLoad(id, null, taskFlowId, name, icon, description, helpTopicId, inDialog, params);
       TaskFlowUtils.raiseEvent("raiseTaskFlowLaunchEvent", jsonPayLoad);
    }

    
}

