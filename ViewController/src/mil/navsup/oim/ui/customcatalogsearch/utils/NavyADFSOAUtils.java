package mil.navsup.oim.ui.customcatalogsearch.utils;

import javax.el.MethodExpression;
import com.thortech.util.logging.Logger;
import oracle.iam.ui.platform.model.common.SOAClientFactory;
import oracle.iam.ui.platform.utils.FacesUtils;

// you can use the SOAClientFactory the same as you can the OIMClientFactory. see the code in
// see the getViewCount method in oracle.iam.ui.platform.view.backing.LeftNavigation to see how to use the SOAClientFactory.
// so far we have not needed it.

public class NavyADFSOAUtils
{
    private static Logger logger = Logger.getLogger(NavyADFSOAUtils.class.getName());
    public NavyADFSOAUtils()
    {
        super();
    }
    
    public static int getPendingApprovalTasksCount() throws Exception
    {
        int numPending = 0;
        logger.debug("getPendingApprovalTasksCount");
        // String viewId = getViewId("PENDING_APPROVALS_VIEW");
        // int viewCount = getViewCount(viewId);
        logger.debug("Calling:" + "getViewId");
        Object obj = FacesUtils.getValueFromELExpression("#{pageFlowScope.dashboardNavigationStateBean.pendingApprovalsCount}");
        logger.debug("Got count:" + obj);
        //logger.debug("Class:" + obj.getClass().getName());
        if (obj != null && obj instanceof Integer)
        {
            numPending = ((Integer)obj).intValue();
        }
        return numPending;
    }
}
