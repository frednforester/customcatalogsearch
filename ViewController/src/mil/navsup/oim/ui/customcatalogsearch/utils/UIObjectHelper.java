package mil.navsup.oim.ui.customcatalogsearch.utils;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;
import javax.faces.context.FacesContext;

import mil.navsup.oim.ui.customcatalogsearch.backing.AccessRequestBean;

@SuppressWarnings({"rawtypes","deprecation"})
public class UIObjectHelper {
    private static ELContext getELContext() {
        return FacesContext.getCurrentInstance().getELContext();
    }

    private static ExpressionFactory getExpressionFactory() {
        return FacesContext.getCurrentInstance().getApplication().getExpressionFactory();
    }

    /**
     * Creates an EL value expression.
     */
    public static ValueExpression createValueExpression(String el, Class clazz) {
        ExpressionFactory factory = getExpressionFactory();
        ELContext context = getELContext();
        return factory.createValueExpression(context, el, clazz);
    }

    /**
     * Returns the object defined using EL expression.
     */
    public static <T> T getContextObject(String el, Class<T> clazz) {
        ValueExpression exp = createValueExpression(el, clazz);
        if (exp == null)
            return null;
        return (T)exp.getValue(getELContext());
    }
    
    public static AccessRequestBean getAccessRequestBean() {
       
         AccessRequestBean accessRequestBean = (AccessRequestBean) NavyADFUtils.getPageFlowScopeParameter("AccessRequestBean");
        return accessRequestBean;
     }

}
