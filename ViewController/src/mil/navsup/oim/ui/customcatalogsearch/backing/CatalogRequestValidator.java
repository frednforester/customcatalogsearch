package mil.navsup.oim.ui.customcatalogsearch.backing;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import mil.navsup.oim.ui.customcatalogsearch.model.eo.RequestedCatalogItem;
import mil.navsup.oim.ui.customcatalogsearch.model.eo.RequestedUser;
import mil.navsup.oim.ui.customcatalogsearch.model.vo.StaticSQL;
import mil.navsup.oim.ui.customcatalogsearch.utils.NavyADFOIMUtils;
import mil.navsup.oim.ui.customcatalogsearch.utils.UIObjectHelper;

import oracle.adf.share.logging.ADFLogger;

import oracle.iam.catalog.vo.OIMType;
import oracle.iam.identity.rolemgmt.vo.Role;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.provisioning.vo.ApplicationInstance;
import oracle.iam.ui.platform.utils.FacesUtils;


public class CatalogRequestValidator {
    
    private ADFLogger logger = ADFLogger.createADFLogger(CatalogRequestValidator.class);
    
    private List<String> ValidAccountStatus = Arrays.asList(new String[] {"Provisioned","Enabled","Provisioning","Disabled"});
    private boolean cartItemsAlreadyProvisioned;
    private final String SYSADMIN = "SYSTEM ADMINISTRATORS";
    
    public CatalogRequestValidator() {
        super();
    }
    
    public StringBuilder validateRequest() {
        
        
        StringBuilder message = this.checkForCommonEntitiesInRequest();
        if (message.length() > 0)
            return message;
        
        
        
        message = this.checkforDuplicateResource();
        if (message.length() > 0)
            return message;
        
        message = this.checkBeneficiaryAndRequesterOperRoles();
        if (message.length() > 0)
            return message;
        
        String entityType = this.getCommonRequestedEntityType();
        logger.finest("Common Type:" + entityType);
        if (OIMType.Entitlement.getValue().equalsIgnoreCase(entityType)) {
            message = this.checkAllUsersHaveAccountsForEntitlement();
        }
        return message;
    }
    
    private StringBuilder checkForCommonEntitiesInRequest() {
        
        logger.finest("checkForCommonEntitiesInRequest");
        StringBuilder message = new StringBuilder();
        List<RequestedCatalogItem> selectedEntities = this.getSelectedEntitiesInCart();
        
        int[] entityTypeCntr = new int[3];
        
        
        for(RequestedCatalogItem entity : selectedEntities)
        {
            logger.finest("Entity:" + entity);
            logger.finest("OIMAI Type:" + OIMType.ApplicationInstance.getValue());
            if (entity.getEntityType().equals(OIMType.ApplicationInstance.getValue())) {
                entityTypeCntr[0]++;
            }
            if (entity.getEntityType().equals(OIMType.Entitlement.getValue())) {
                entityTypeCntr[1]++;
            }
            if (entity.getEntityType().equals(OIMType.Role.getValue())) {
                entityTypeCntr[2]++;
            }
        }
        
        int hitCntr = 0;
        for(int i=0;i<entityTypeCntr.length;i++)
        {
            if (entityTypeCntr[i] > 0) {
                hitCntr++;
            }
        }
    
        if (hitCntr > 1) {
            message.append("<p><b>Items in cart must all be of the same entity type</b></p>");
        }
        
        return message;
        
    }
    
    public String getCommonRequestedEntityType() {
        List<RequestedCatalogItem> selectedEntities = this.getSelectedEntitiesInCart();
        int[] entityTypeCntr = new int[3];
        int appCntr = 0;
        int entCntr = 0;
        int roleCntr = 0;
        
        for(RequestedCatalogItem entity : selectedEntities)
        {
            logger.finest("Entity:" + entity);
            
            if (entity.getEntityType().equals(OIMType.ApplicationInstance.getValue())) {
                appCntr++;
            }
            if (entity.getEntityType().equals(OIMType.Entitlement.getValue())) {
                entCntr++;
            }
            if (entity.getEntityType().equals(OIMType.Role.getValue())) {
                roleCntr++;
            }
        }
        logger.finest("appCntr:" + appCntr);
        logger.finest("entCntr:" + entCntr);
        logger.finest("roleCntr:" + roleCntr);
        if (appCntr > 0) {
            return OIMType.ApplicationInstance.getValue();
        }
        if (entCntr > 0) {
            return OIMType.Entitlement.getValue();
        }
        if (roleCntr > 0) {
            return OIMType.Role.getValue();
        }
        return "";
    }
    
    private StringBuilder checkAllUsersHaveAccountsForEntitlement() {
        
        StringBuilder message = new StringBuilder();
        List<RequestedUser> selectedUsers = this.getSelectedUsersInCart();
        if (selectedUsers == null || selectedUsers.isEmpty())
        {
            logger.finest("No Users Selected");
            return null;
        }
        
        List<RequestedCatalogItem> selectedEntities = this.getSelectedEntitiesInCart();
        
        for(RequestedCatalogItem entity : selectedEntities)
        {
            logger.finest("Entity:" + entity);
        }
        
            for(RequestedCatalogItem entity : selectedEntities)
            {
                logger.finest("Entity:" + entity);
                if (entity.getEntityType().equals(OIMType.Entitlement.getValue())) {
                    
                    
                }
                
            }
        
        
        return message;
        
    }
    
    private StringBuilder checkforDuplicateResource()
    {   
        NavyADFOIMUtils oimUtils = new NavyADFOIMUtils();
        List<RequestedUser> selectedUsers = this.getSelectedUsersInCart();
        if (selectedUsers == null || selectedUsers.isEmpty())
        {
            logger.finest("No Users Selected");
            return null;
        }
        
        List<RequestedCatalogItem> selectedEntities = this.getSelectedEntitiesInCart();
        
        for(RequestedCatalogItem entity : selectedEntities)
        {
            logger.finest("Entity:" + entity);
        }
        
        StringBuilder message = new StringBuilder();
        StringBuilder provisionedLines = new StringBuilder();
        StringBuilder requestedLines = new StringBuilder();
        StringBuilder missingAppLines = new StringBuilder();
        StringBuilder missingAppNames = new StringBuilder();
        for(RequestedUser u : selectedUsers)
        {   
            logger.finest("Checking user for duplicates:" + u );
            //StringBuilder message = new StringBuilder("<html><body>");
            String beneficiaryDisplayName = u.getDisplayName();
            Map provisionedMap = oimUtils.getUserProvisionedItems(u.getUsrKey(),this.ValidAccountStatus);
            logger.finest("ProvisionedMap:" + provisionedMap);
            logger.finest("Processing:" + beneficiaryDisplayName + ":" + u.getLogin());      
            for (RequestedCatalogItem entity : selectedEntities) {
                String entityId = (String)entity.getEntityKey();
                String entityType = (String)entity.getEntityType();
                String entityDisplayName = (String)entity.getEntityDisplayName();
                logger.finest("entityKey:" + entityId);
                logger.finest("entityType:" + entityType);
                logger.finest("entityDisplayName:" + entityDisplayName);
                boolean isProvisioned = this.isEntityProvisioned(provisionedMap, entityType,entityId);
                if(isProvisioned){
                    provisionedLines.append("<p><li>"+entityDisplayName+"</li></p>");
                }
                if (entityType.equalsIgnoreCase(OIMType.Entitlement.getValue()) && !isProvisioned)
                {
                    isProvisioned = this.isAppProvisionedForEntitlement(provisionedMap, entityType, entityId,missingAppNames);
                    if (!isProvisioned) {
                        missingAppLines.append("<p><li>App Name:" + missingAppNames.toString()  + ":" + entityDisplayName+"</li></p>");
                        missingAppNames.setLength(0);
                    }
                }
                
                List<String> duplicateIds = new ArrayList<String>();
                boolean isRequested = this.isEntityRequested(entityType, entityId, u.getUsrKey(),duplicateIds);
                if(isRequested){
                    for(String duplicate : duplicateIds) {
                        requestedLines.append("<p><li>" + "Type:" + entityType + " Name:" + entityDisplayName + " RequestID:" + duplicate + "</li></p>");
                    }
                }
            }

            if (missingAppLines.length() > 0)
            {
                message.append("<p><b>Following Entitlements require the Application to be provisioned first</b></p>");
                message.append(missingAppLines.toString());
                missingAppLines.setLength(0);
            }

            if (provisionedLines.length() > 0)
            {
                message.append("<p><b>Following Cart Items are already provisioned to User: "+beneficiaryDisplayName+"</b></p>");
                message.append(provisionedLines.toString());
                provisionedLines.setLength(0);
            }
            if (requestedLines.length() > 0)
            {
                message.append("<p><b>Following Cart Items have already been requested for User: "+beneficiaryDisplayName+"</b></p>");
                message.append(requestedLines.toString());
                requestedLines.setLength(0);
            }
        }
        return message;
    }
    
    private List<RequestedUser> getSelectedUsersInCart()
    {
        
        List<RequestedUser> targetUsers = UIObjectHelper.getAccessRequestBean().getSelectedUsers();
        
        if (targetUsers == null)
        {
            logger.finest("No Selected Users");
        }
        
        return targetUsers;
    }
    
    private List<RequestedCatalogItem> getSelectedEntitiesInCart()
    {
        
        List<RequestedCatalogItem> targetEntities = UIObjectHelper.getAccessRequestBean().getSelectedItems();
        
        if (targetEntities == null || targetEntities.isEmpty())
        {
            logger.finest("No Selected Entities");
        }
        
        return targetEntities;
    }
    
    private String getSelectedNDPRole()
    {
        
        String selectedSearchRole = UIObjectHelper.getAccessRequestBean().getSelectedNDPRole();
        return selectedSearchRole;
    }
    
    
    private StringBuilder checkBeneficiaryAndRequesterOperRoles()
    {
        StringBuilder message = new StringBuilder();
        StringBuilder invalidUsers = new StringBuilder();
        NavyADFOIMUtils oimUtils = new NavyADFOIMUtils();
        String usrKey = null;
        String selectedNBISRole = this.getSelectedNDPRole();
        logger.finest("selectedNBISRole:" + selectedNBISRole);
        if (selectedNBISRole == null || selectedNBISRole.isEmpty() || selectedNBISRole.equals("0")) {
            selectedNBISRole = "ALL";
        }
        logger.finest("selectedNBISRole:" + selectedNBISRole);
        //
        boolean pdaSelected = false;
        boolean pdaEntitlment = false;
        boolean pdaApp = false;
        boolean appSelected = false;
        
        List<RequestedCatalogItem> selectedEntities = this.getSelectedEntitiesInCart();
        
        Map<String,String> pdaEntitlementMap = new HashMap<String,String>();
        
        for(RequestedCatalogItem entity : selectedEntities)
        {
            String entityId = (String)entity.getEntityKey();
            String entityType = (String)entity.getEntityType();
            String entityDisplayName = (String)entity.getEntityDisplayName();
            logger.finest("Checking for PDA request");
            try
            {
                if (entityType.equalsIgnoreCase(OIMType.Entitlement.getValue()))
                {   
                    if (oimUtils.isEntitlementPDA(entityId))
                    {
                        pdaSelected = true;
                        pdaEntitlment = true;
                        pdaEntitlementMap.put(entityId,entityDisplayName);
                    }    
                }
                if (entityType.equalsIgnoreCase(OIMType.ApplicationInstance.getValue()))
                {   
                    appSelected = true;
                    if (oimUtils.isAppInstancePDA(entityId))
                    {
                        pdaApp = true;
                        pdaSelected = true;
                    }    
                }
            }
            catch(Exception e)
            {
                logger.severe("APIError:" + e.getMessage(),e);
            }  
            
        }
        
        Object obj = FacesUtils.getValueFromELExpression("#{oimcontext.currentUser.usr_key}");
        if (obj != null)
            usrKey = obj.toString();
        if (usrKey == null)
        {
            logger.finest("No Current User");
            logger.severe("No Current User");
            return message;
        }
        
        
        List<RequestedUser> beneficiaries = this.getSelectedUsersInCart();
        if (beneficiaries == null || beneficiaries.isEmpty())
        {
            logger.finest("No Users Selected");
            return message;
        }
        
        if (appSelected) {
            Set<String> retAttrs = new HashSet<String>();
            retAttrs.add("ORGSTATUS");
            for(RequestedUser reqbene : beneficiaries)
            {
                String beneKey = reqbene.getUsrKey();
                Long beneKeyL = Long.valueOf(beneKey);
                try {
                    User bene = oimUtils.getUser(beneKeyL, null);
                    String orgStatus = (String)bene.getAttribute("ORGSTATUS");
                    if (!"Approved".equalsIgnoreCase(orgStatus)) {
                        String userInfo = bene.getDisplayName() + ":" + orgStatus;
                        invalidUsers.append("<p><li>").append(userInfo).append("</li></p>");
                    }
                }
                catch(Exception e) {
                    logger.severe("Error getting User:" + reqbene);
                    logger.severe("Error getting User:" + e.getMessage(),e);
                }
            }
            if (invalidUsers.length() > 0)
            {
                message.append("<p><b>All beneficiaries must have an Approved Organization Status before an Application can be requested</b></p>");
                message.append(invalidUsers.toString());    
                invalidUsers.setLength(0);
            }
        }
        
        logger.finest("Is PDA request:" + pdaSelected);
        if (!pdaSelected)
        {
            logger.finest("No PDA Resources requested");
            return message;
        }
        
        if (!pdaEntitlementMap.isEmpty()) {
            pdaEntitlementMap = oimUtils.getEntitlementMetaData(pdaEntitlementMap);
            logger.finest("pdaEntitlementMap:" + pdaEntitlementMap);
        }
        
        /*
        Map requesterRoles = null;
        try
        {
            requesterRoles = oimUtils.getAllUsersOperRoles(usrKey);
            //allow if sysadmin
            if (requesterRoles.containsValue(this.SYSADMIN))
                return message;
        }
        catch(Exception e)
        {
            logger.severe("APIError:" + e.getMessage(),e);
            return message;
        }
        */
        
        logger.finest("Checking roles and orgs");
        Set uniqueRoles = new HashSet();
        Set<String> uniqueOrgs = new HashSet();
        
        for(RequestedUser bene : beneficiaries)
        {
            String beneKey = bene.getUsrKey();
            try
            {
                logger.finest("Bene:" + beneKey);
                String orgName = oimUtils.getUsersOrgName(beneKey);
                uniqueOrgs.add(orgName);
                logger.finest("uniqueOrgs:" + uniqueOrgs);
                if (uniqueOrgs.size() > 1) {
                    invalidUsers.append(("<p><li>All beneficiaries in this request must have the same Organization</li></p>"));
                }
                Map<String,String> opRoles = oimUtils.getAllUsersOperRoles(beneKey);
                logger.finest("opRoles:" + opRoles);
                if (!"All".equalsIgnoreCase(selectedNBISRole)) {
                    if (!opRoles.containsKey(selectedNBISRole))
                    {
                        invalidUsers.append(("<p><li>Beneficiary has incorrect Ops Role for this request: " + bene.getDisplayName() + "</li></p>"));
                    }
                    String strRole = opRoles.get(selectedNBISRole);
                    logger.finest("Check for valid pda ent:" + strRole);
                    logger.finest("Check for valid pda ent:" + orgName);
                    boolean isvalidEnt = isValidEntitlementForUser(strRole, orgName,pdaEntitlementMap);
                    if (!isvalidEnt) {
                        invalidUsers.append(("<p><li>Beneficiary has incorrect Organization or Ops Role for this request: " + bene.getDisplayName() + "</li></p>"));
                    }
                } else // at least one op role needs to be valid for the entitlement.
                {
                    boolean isValid = false;
                    for (String opRole : opRoles.values()) {
                        boolean isvalidEnt = isValidEntitlementForUser(opRole, orgName,pdaEntitlementMap);
                        logger.finest("isvalidEnt:" + isvalidEnt);
                        if (isvalidEnt)
                            isValid = true;
                    }
                    if (!isValid) {
                        invalidUsers.append(("<p><li>Invalid Organization or Ops Role for this request: " + bene.getDisplayName() + "</li></p>"));    
                    }
                }

            }
            catch(Exception e)
            {
                logger.severe("APIError:" + e.getMessage(),e);
            }
        }
        if (invalidUsers.length() > 0)
        {
            message.append("<p><b>The following users do not have a matching operational role or organization for this request</b></p>");
            message.append(invalidUsers.toString());
            invalidUsers.setLength(0);
        }
        return message;
    }
    
    
    private boolean isEntityProvisioned(Map provisionedMap, String entityType, String entityId) {
        logger.finest("isEntityProvisioned");
        boolean isProvisioned = false;
        if (provisionedMap != null && entityType != null) {
            if (entityType.equalsIgnoreCase(OIMType.Role.getValue())) {
                List<String> roleList = (List<String>)provisionedMap.get(OIMType.Role.getValue());
                if (roleList != null)
                    isProvisioned = roleList.contains(entityId);
            } else if (entityType.equalsIgnoreCase(OIMType.Entitlement.getValue())) {
                List<String> entitlementList = (List<String>)provisionedMap.get(OIMType.Entitlement.getValue());
                if (entitlementList != null)
                {
                    isProvisioned = entitlementList.contains(entityId);
                }
            } else if (entityType.equalsIgnoreCase(OIMType.ApplicationInstance.getValue())) {
                List<String> applicationList = (List<String>)provisionedMap.get(OIMType.ApplicationInstance.getValue());
                if (applicationList != null)
                    isProvisioned = applicationList.contains(entityId);
            }
        }
        return isProvisioned;
    }
    
    
    private boolean isAppProvisionedForEntitlement(Map provisionedMap, String entityType, String entityId, StringBuilder missingApps) {
        
        if (!entityType.equalsIgnoreCase(OIMType.Entitlement.getValue())) {
            return true;
        }
        
        NavyADFOIMUtils oimUtils = new NavyADFOIMUtils();
        
        try
        {
            ApplicationInstance ai = oimUtils.getApplicationKeyForEntitlement(entityId);
            logger.finest("does user have:" + ai.getApplicationInstanceName());
            List<String> applicationList = (List<String>)provisionedMap.get(OIMType.ApplicationInstance.getValue());
            logger.finest("Users Applications:" + applicationList);
            Long appKey = ai.getApplicationInstanceKey();
            if (applicationList.contains(appKey.toString())) {
                return true;
            }
            missingApps.append(ai.getApplicationInstanceName());
            
        }
        catch(Exception e) {
            logger.severe("Failed to get entitlement with API:" + e.getMessage(),e);
            
        }
        return false;
        
    }

    
    
    private boolean isEntityRequested(String entityType, String entityId,String userKey,List<String> duplicateIds) {
        DataSource ds = null;
        ResultSet rs = null;
        PreparedStatement stmnt = null;
        Connection conn = null;
        logger.finest("isEntityRequested");
        boolean isRequested = false;
        
        NavyADFOIMUtils oimUtils = new NavyADFOIMUtils();
        try
        {
            ds = oimUtils.getCustomDS(StaticSQL.APPICATIONDATASOURCE);
        }
        catch(Exception e) {
            logger.severe("Error getting datasource:" + StaticSQL.APPICATIONDATASOURCE,e);
        }
        try {
            
            conn = ds.getConnection();
            logger.finest("getrequests:" + StaticSQL.GETUSERSREWQUESTBYENTITY);
            stmnt = conn.prepareStatement(StaticSQL.GETUSERSREWQUESTBYENTITY);
            stmnt.setString(1,userKey);
            stmnt.setString(2,entityId);
            rs = stmnt.executeQuery();
            while(rs.next()) {
                logger.finest("current request for user");
                logger.finest("Beneficiary:" + rs.getString("RBE_BENEFICIARY_KEY"));
                logger.finest("Entityname:" + rs.getString("RBE_ENTITY_NAME"));
                logger.finest("Duplicate RequestId:" + rs.getString("REQUEST_ID"));
                isRequested = true;
                if (duplicateIds != null)
                    duplicateIds.add(rs.getString("REQUEST_ID"));
            }
        }
        catch(Exception e) {
            logger.severe("Error getting requests:" + e.getMessage(),e);
        }
        finally {
            try
            {
            if (rs != null)
                rs.close();
            if (stmnt != null)
                stmnt.close();
            if (conn != null)
                conn.close();
            }
            catch(Exception e) {
                
            }
        }
        return isRequested;
    }

   
    
    private boolean isValidEntitlementForUser(String opsRole, String orgName,Map<String, String> entitlements) throws Exception {
        logger.finest(("Validating PDA Entitlements:" + entitlements));
        logger.finest(("Validating PDA Entitlements:" + opsRole));
        logger.finest(("Validating PDA Entitlements:" + orgName));
        try {
            Set<String> keys = entitlements.keySet();
            for (String entName : keys) {
                String value = entitlements.get(entName);
                if (value == null || value.trim().isEmpty()) {
                    logger.severe("Invalid PDA Entitlement. Metadata Missing:" +
                                 entName);
                    return false;
                }
                if (value.startsWith("[") && value.endsWith("]") &&
                    value.contains("][")) {
                    if (!value.contains(orgName)) {
                        return false;
                    }
                    if (!value.contains(opsRole)) {
                        return false;
                    }
                } else {
                    logger.severe("Invalid PDA Entitlement. Metadata Invalid:" +
                                 entName);
                    return false;
                }

            }
        } catch (Exception e) {
            logger.severe("APIError:" + e.getMessage(), e);
            throw e;
        }
        return true;
    }
    

}
