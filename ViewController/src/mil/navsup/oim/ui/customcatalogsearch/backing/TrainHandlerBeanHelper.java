package mil.navsup.oim.ui.customcatalogsearch.backing;

import java.io.Serializable;

public class TrainHandlerBeanHelper implements Serializable {
    public TrainHandlerBeanHelper() {
        super();
    }

    private static final long serialVersionUID = 1L;
    private String selectedTrainStopOutcome = null;


    public void setSelectedTrainStopOutcome(String selectedTrainStopOutcome) {
        this.selectedTrainStopOutcome = selectedTrainStopOutcome;
    }


    public String getSelectedTrainStopOutcome() {
        return this.selectedTrainStopOutcome;
    }
}
