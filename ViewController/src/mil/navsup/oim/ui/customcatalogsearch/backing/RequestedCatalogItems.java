package mil.navsup.oim.ui.customcatalogsearch.backing;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import mil.navsup.oim.ui.customcatalogsearch.model.AppModuleImpl;
import mil.navsup.oim.ui.customcatalogsearch.model.eo.RequestedCatalogItem;
import mil.navsup.oim.ui.customcatalogsearch.model.vo.CatalogItemSearchVOCriteria;
import mil.navsup.oim.ui.customcatalogsearch.model.vo.ProgramaticCatalogViewObjImpl;
import mil.navsup.oim.ui.customcatalogsearch.utils.NavyADFUtils;
import mil.navsup.oim.ui.customcatalogsearch.utils.NavyFacesUtils;
import mil.navsup.oim.ui.customcatalogsearch.utils.UIObjectHelper;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.data.RichTable;

import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adfinternal.view.faces.model.binding.FacesCtrlHierNodeBinding;

import oracle.jbo.Key;
import oracle.jbo.ViewObject;
import oracle.jbo.uicli.binding.JUCtrlHierBinding;
import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import org.apache.myfaces.trinidad.event.AttributeChangeEvent;
import org.apache.myfaces.trinidad.event.SelectionEvent;
import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.RowKeySet;

public class RequestedCatalogItems {
    
    private ADFLogger logger = ADFLogger.createADFLogger(RequestedCatalogItems.class);
 
    private String searchDisplayName;
    private String searchEntityType;
    private String searchApplicationName;
    private AccessRequestBean accessRequestBean = null;
   
    private List<RequestedCatalogItem> requesteCatalogItemList;
    private FacesCtrlHierNodeBinding  selectedItemRow;
    private RequestedCatalogItem deletedItemRow;

    private String renderApplicationSelect;
    private RichPanelFormLayout catalogSearchFormBinding;

    public RequestedCatalogItems() {
        logger.finest("CustomCatalogSearch Started");
        

    }

    public void startSearch(ActionEvent actionEvent) {
        
        logger.finest("startSearch");
        logger.finest("Parms:" + this.searchEntityType + " :" + this.searchDisplayName);
 
        AppModuleImpl am = (AppModuleImpl) NavyFacesUtils.resolvElDC("AppModuleDataControl");
        if (am == null) {
            logger.finest("AM NULL!");
            return;
        }
        
        ProgramaticCatalogViewObjImpl catVO = am.getProgramaticCatalogViewObj1();
        
        CatalogItemSearchVOCriteria criteria = new CatalogItemSearchVOCriteria();
        
        criteria.setEntityDisplayName(this.searchDisplayName);
        criteria.setEntityType(this.searchEntityType);
        criteria.setApplicationName(this.searchApplicationName);
        
        if (!this.validateCriteria(criteria)) {
            return;
        }
        
        
        
        catVO.setCriteria(criteria);
        catVO.executeQuery();
        
        if (criteria.getReturnMessage() != null) {
            FacesMessage fm = new FacesMessage(criteria.getReturnMessage());
            fm.setSeverity(FacesMessage.SEVERITY_WARN);
            FacesContext fctx = FacesContext.getCurrentInstance();
            fctx.addMessage(null, fm);
        }
        
        
        List<RequestedCatalogItem> reqCatItems = catVO.getRequesteCatalogItemList();
        logger.finest("reqCatItems:" + reqCatItems.size());
    }
    
    public void selectItemCB(ActionEvent actionEvent) {
        
        logger.finest("selectItemCB");
        logger.finest("selectedRow:" + this.selectedItemRow);
        String[] names = this.selectedItemRow.getAttributeNames();
        for(String name : names) {
            logger.finest("Row Attr:" + name);
            logger.finest("row val:" + this.selectedItemRow.getAttribute(name));
        }
        RequestedCatalogItem item = new RequestedCatalogItem();
        //logger.finest("selected usrKey:" + usrKey);
        item.setEntityName((String)selectedItemRow.getAttribute("Entityname"));
        item.setEntityDisplayName((String)selectedItemRow.getAttribute("EntityDisplayname"));
        item.setEntityType((String)selectedItemRow.getAttribute("Entitytype"));
        item.setEntityKey((String)selectedItemRow.getAttribute("Entitykey"));
        item.setId((String)selectedItemRow.getAttribute("Id"));
        logger.finest("Adding item to selected:" + item);
        getAccessRequestBean().addItemById(item);
       //getAccessRequestBean().updateAutoMaticallyAddedCartItemOnChangeOfUserSelection();
       //getAccessRequestBean().updateCountUI();
        NavyADFUtils.partialRender(getAccessRequestBean().getSelectedItemsTable());
        NavyADFUtils.partialRender(getAccessRequestBean().getAvailableItems());
     }
    
    public void removeItemCB(ActionEvent actionEvent) {
        
        logger.finest("removeItemCB");
         logger.finest("remove selectedRow:" + this.deletedItemRow);
        
        
        getAccessRequestBean().removeItemById(this.deletedItemRow);
       //getAccessRequestBean().updateAutoMaticallyAddedCartItemOnChangeOfUserSelection();
       //getAccessRequestBean().updateCountUI();
        NavyADFUtils.partialRender(getAccessRequestBean().getSelectedItemsTable());
        NavyADFUtils.partialRender(getAccessRequestBean().getAvailableItems());
     }
    
    public void resetSearch(ActionEvent actionEvent) {
        logger.finest("resetSearch");
        AppModuleImpl am = (AppModuleImpl) NavyFacesUtils.resolvElDC("AppModuleDataControl");
        ViewObject usersVO = am.getProgramaticCatalogViewObj1();
        ViewObject catalogSearchRVO = am.getCatalogSearchFormVO1();
        usersVO.executeEmptyRowSet();
        catalogSearchRVO.executeQuery();
        getAccessRequestBean().getSelectedItems().clear();
        NavyADFUtils.partialRender(getAccessRequestBean().getSelectedItemsTable());
        NavyADFUtils.partialRender(getAccessRequestBean().getAvailableItems());

    }
    
    
    private boolean validateCriteria(CatalogItemSearchVOCriteria parmMap) {
        
        String displayName = (String) parmMap.getEntityDisplayName();
        if (displayName != null && displayName.trim().isEmpty())
            displayName = null;
        String entityType = (String) parmMap.getEntityType();
        if (entityType != null && entityType.trim().isEmpty())
            entityType = null;
        
        if (displayName == null && entityType == null ) {
            FacesMessage fm = new FacesMessage("Please specify at least one search value");
            fm.setSeverity(FacesMessage.SEVERITY_WARN);
            FacesContext fctx = FacesContext.getCurrentInstance();
            fctx.addMessage(null, fm);
            return false;
        }
        return true;

        
    }
    
    /*
     * the available user selection. mark the row selected when clicked
     */
    public void onAvailTableSelect(SelectionEvent selectionEvent) {
        
        logger.finest("onAvailTableSelect");
        RichTable table = (RichTable) selectionEvent.getSource();
        CollectionModel tableModel = (CollectionModel) table.getValue();
        JUCtrlHierBinding adfTableBinding = (JUCtrlHierBinding) tableModel.getWrappedData();
        DCIteratorBinding tableIteratorBinding = adfTableBinding.getDCIteratorBinding();
        Object selectedRowData = table.getSelectedRowData();
        JUCtrlHierNodeBinding nodeBinding = (JUCtrlHierNodeBinding) selectedRowData;
        Key rwKey = nodeBinding.getRowKey();
        logger.finest("setting current row:" + rwKey.toString());
        tableIteratorBinding.setCurrentRowWithKey(rwKey.toStringFormat(true));
        
    }
    
    /*
     *  mark the row selected when selected users table clicked
     */
    public void onSelectedItemTableSelect(SelectionEvent selectionEvent) {
        
        logger.finest("onSelectedTableSelect");
        RichTable table = (RichTable) selectionEvent.getSource();
        ArrayList<RequestedCatalogItem> tableModel = (ArrayList<RequestedCatalogItem>)table.getValue();
        //UCtrlHierBinding adfTableBinding = (JUCtrlHierBinding) tableModel.getWrappedData();
        //DCIteratorBinding tableIteratorBinding = adfTableBinding.getDCIteratorBinding();
        
        //Object selectedRowData = table.getSelectedRowData();
        //JUCtrlHierNodeBinding nodeBinding = (JUCtrlHierNodeBinding) selectedRowData;
        //Key rwKey = nodeBinding.getRowKey();
        RowKeySet rowKeySet = table.getSelectedRowKeys();
        logger.finest("Rowkeyset:" + rowKeySet);
        
        rowKeySet.clear();
        //SelectionEvent selectEvent = new SelectionEvent(table.getSelectedRowKeys(),rowKeySet,table);
        //selectEvent.queue();
        //tableIteratorBinding.setCurrentRowWithKey(rwKey.toStringFormat(true));
        
    }
    
    public void checkForEntitlement(ValueChangeEvent valueChangeEvent) {
        
        String value = (String)valueChangeEvent.getNewValue();
        String oldvalue = (String)valueChangeEvent.getOldValue();
        logger.finest("Old:" + oldvalue);
        logger.finest("New:" + value);
        
        if ("Entitlement".equalsIgnoreCase(value)) {
            this.setRenderApplicationSelect("true");
        }
        else {
            this.setRenderApplicationSelect("false");
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.catalogSearchFormBinding);
        // Add event code here...
    }
    
    private AccessRequestBean getAccessRequestBean() {
       if (this.accessRequestBean == null) {
         this.accessRequestBean = (AccessRequestBean) NavyADFUtils.getPageFlowScopeParameter("AccessRequestBean");
       }
       return this.accessRequestBean;
       
     }
    
    public void setSearchDisplayName(String searchDisplayName) {
        this.searchDisplayName = searchDisplayName;
    }

    public String getSearchDisplayName() {
        return searchDisplayName;
    }

    public void setSearchEntityType(String searchEntityType) {
        this.searchEntityType = searchEntityType;
    }

    public String getSearchEntityType() {
        return searchEntityType;
    }

    public void setRequesteCatalogItemList(List<RequestedCatalogItem> requesteCatalogItemList) {
        this.requesteCatalogItemList = requesteCatalogItemList;
    }

    public List<RequestedCatalogItem> getRequesteCatalogItemList() {
        return requesteCatalogItemList;
    }

    public void setSelectedItemRow(FacesCtrlHierNodeBinding selectedItemRow) {
        logger.finest("setSelectedItemRow:" + selectedItemRow);
        this.selectedItemRow = selectedItemRow;
    }

    public FacesCtrlHierNodeBinding getSelectedItemRow() {
        return selectedItemRow;
    }


    public void setDeletedItemRow(RequestedCatalogItem deletedItemRow) {
        this.deletedItemRow = deletedItemRow;
    }

    public RequestedCatalogItem getDeletedItemRow() {
        return deletedItemRow;
    }


    public void setRenderApplicationSelect(String renderApplicationSelect) {
        this.renderApplicationSelect = renderApplicationSelect;
    }

    public String getRenderApplicationSelect() {
        logger.finest("getRenderApplicationSelect:" + this.renderApplicationSelect);
        return renderApplicationSelect;
    }


    public void setCatalogSearchFormBinding(RichPanelFormLayout catalogSearchFormBinding) {
        this.catalogSearchFormBinding = catalogSearchFormBinding;
    }

    public RichPanelFormLayout getCatalogSearchFormBinding() {
        return catalogSearchFormBinding;
    }


    public void setSearchApplicationName(String searchApplicationName) {
        this.searchApplicationName = searchApplicationName;
    }

    public String getSearchApplicationName() {
        return searchApplicationName;
    }
}
