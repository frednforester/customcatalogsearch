package mil.navsup.oim.ui.customcatalogsearch.backing;


import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import mil.navsup.oim.ui.customcatalogsearch.model.AppModuleImpl;
import mil.navsup.oim.ui.customcatalogsearch.model.eo.RequestedUser;
import mil.navsup.oim.ui.customcatalogsearch.model.vo.ProgramaticUserViewObjImpl;
import mil.navsup.oim.ui.customcatalogsearch.model.vo.ProgramaticUserViewObjRowImpl;
import mil.navsup.oim.ui.customcatalogsearch.model.vo.UserSearchVOCriteria;
import mil.navsup.oim.ui.customcatalogsearch.utils.NavyADFUtils;
import mil.navsup.oim.ui.customcatalogsearch.utils.NavyFacesUtils;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.adfinternal.view.faces.model.binding.FacesCtrlHierNodeBinding;

import oracle.jbo.Key;
import oracle.jbo.ViewObject;
import oracle.jbo.uicli.binding.JUCtrlHierBinding;
import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import org.apache.myfaces.trinidad.event.SelectionEvent;
import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.RowKeySet;


public class RequestedUsers {

    private RichInputText displayNameBind;
    private RichInputText userLoginBind;
    private RichInputText emailBinding;
    private RichSelectOneChoice roleKeyBinding;
    private RichSelectOneChoice orgKeyBinding;
    private String roleKey;
    private String orgKey;
    
    private AccessRequestBean accessRequestBean = null;
    private String selectedUser;
    private FacesCtrlHierNodeBinding  selectedUserRow;
    private RequestedUser deletedUserRow;
    
    private List<RequestedUser> requestedUsers;
    
    
    private ADFLogger logger = ADFLogger.createADFLogger(RequestedUsers.class);


    public RequestedUsers() {
        
        logger.finest("Constructor RequestedUsers");
    }

    

    /*
     *  Search button for search users
     */
    public void searchAL(ActionEvent actionEvent) {
        logger.finest("searchAL");
        AppModuleImpl am = (AppModuleImpl) NavyFacesUtils.resolvElDC("AppModuleDataControl");
        if (am == null) {
            logger.finest("AM NULL!");
            return;
        }
        
        
        ProgramaticUserViewObjImpl usersVO = am.getProgramaticUserViewObj1();
        UserSearchVOCriteria criteria = new UserSearchVOCriteria();
        criteria.setDisplayName((String)this.displayNameBind.getValue());
        criteria.setEmail((String)this.emailBinding.getValue());
        criteria.setUserLogin((String)this.userLoginBind.getValue());
        criteria.setRoleKey(this.roleKey);
        criteria.setOrgKey(this.orgKey);
        criteria.setMaxRecords(50);
        if (!this.validateCriteria(criteria)) {
            return;
        }
        
        
        
        usersVO.setCriteria(criteria);
        usersVO.executeQuery();
        if (criteria.getReturnMessage() != null) {
            FacesMessage fm = new FacesMessage(criteria.getReturnMessage());
            fm.setSeverity(FacesMessage.SEVERITY_WARN);
            FacesContext fctx = FacesContext.getCurrentInstance();
            fctx.addMessage(null, fm);
        }
        requestedUsers = usersVO.getRequestedUserList();
        this.getAccessRequestBean().setAvailableUsersList(requestedUsers);
        this.getAccessRequestBean().setSelectedNDPRole(this.roleKey);
        logger.finest("requestedUsers:" + requestedUsers.size());
        
    }

    /*
     * reset the search form and the results
     */
    public void resetAL(ActionEvent actionEvent) {
        AppModuleImpl am = (AppModuleImpl) NavyFacesUtils.resolvElDC("AppModuleDataControl");
        ViewObject usersVO = am.getProgramaticUserViewObj1();
        ViewObject usersSearchRVO = am.getUserSearchFormVO1();
        usersVO.executeEmptyRowSet();
        usersSearchRVO.executeQuery();
        getAccessRequestBean().getSelectedUsers().clear();
        NavyADFUtils.partialRender(getAccessRequestBean().getSelectedUsersTable());
        NavyADFUtils.partialRender(getAccessRequestBean().getAvailableUsers());
        
        
    }
    
    
    private AccessRequestBean getAccessRequestBean() {
       if (this.accessRequestBean == null) {
         this.accessRequestBean = (AccessRequestBean) NavyADFUtils.getPageFlowScopeParameter("AccessRequestBean");
       }
       return this.accessRequestBean;
       
     }
    
    /*
    private AccessRequestBean getRequestNavigationBean() {
       if (this.accessRequestBean == null) {
           
         this.accessRequestBean = (AccessRequestBean) NavyADFUtils.getPageFlowScopeParameter("NavigationHandlerBean");
       }
       return this.accessRequestBean;
       
     }
    */
    
    /*
     * Process the ADD button on the search results table
     */
    public void selectUserCB(ActionEvent actionEvent) {
        
        logger.finest("selectUserCB");
        logger.finest("selectedRow:" + this.selectedUserRow);
        String[] names = this.selectedUserRow.getAttributeNames();
        for(String name : names) {
            logger.finest("Row Attr:" + name);
            logger.finest("row val:" + this.selectedUserRow.getAttribute(name));
        }
        RequestedUser user = new RequestedUser();
        //logger.finest("selected usrKey:" + usrKey);
        user.setDisplayName((String)selectedUserRow.getAttribute("displayName"));
        user.setEmail((String)selectedUserRow.getAttribute("email"));
        user.setUsrKey((String)selectedUserRow.getAttribute("usrKey"));
        user.setAltTokenIndicator((String)selectedUserRow.getAttribute("altTokenIndicator"));
        user.setOrganizationName((String)selectedUserRow.getAttribute("organizationName"));
        logger.finest("Adding user to selected:" + user);
        getAccessRequestBean().addUserById(user);
       //getAccessRequestBean().updateAutoMaticallyAddedCartItemOnChangeOfUserSelection();
       //getAccessRequestBean().updateCountUI();
        NavyADFUtils.partialRender(getAccessRequestBean().getSelectedUsersTable());
        NavyADFUtils.partialRender(getAccessRequestBean().getAvailableUsers());
     }
    
    /*
     * process the delete button on the selected users table
     */
    public void removeUserCB(ActionEvent actionEvent) {
        
        logger.finest("removeUserCB");
         logger.finest("remove selectedRow:" + this.deletedUserRow);
        
        
        getAccessRequestBean().removeUserById(this.deletedUserRow);
       //getAccessRequestBean().updateAutoMaticallyAddedCartItemOnChangeOfUserSelection();
       //getAccessRequestBean().updateCountUI();
        NavyADFUtils.partialRender(getAccessRequestBean().getSelectedUsersTable());
        NavyADFUtils.partialRender(getAccessRequestBean().getAvailableUsers());
     }
    
   
    
    /*
     * find the selected user and add them to the selected list
     */
    private RequestedUser findSelectedRow(String usrKey) {
        
        logger.finest("findSelectedRow");
        RequestedUser u = null;
            
        if (this.selectedUserRow != null) {
            logger.finest("Got a selected Row!");
            ProgramaticUserViewObjRowImpl row = (ProgramaticUserViewObjRowImpl)this.selectedUserRow.getRow();
            logger.finest("Got a selected Row:" + row);
            u = new RequestedUser();
            u.setDisplayName(row.getdisplayName());
            u.setUsrId(row.getusrKey());
            u.setEmail(row.getemail());
            u.setAltTokenIndicator(row.getaltTokenIndicator());
            u.setUsrKey(usrKey);
            logger.finest("selected Row:" + u.getDisplayName());
            return u;
        }
       
        return u;
    }
    
    /*
     * the available user selection. mark the row selected when clicked
     */
    public void onTableSelect(SelectionEvent selectionEvent) {
        
        logger.finest("onTableSelect");
        RichTable table = (RichTable) selectionEvent.getSource();
        CollectionModel tableModel = (CollectionModel) table.getValue();
        JUCtrlHierBinding adfTableBinding = (JUCtrlHierBinding) tableModel.getWrappedData();
        DCIteratorBinding tableIteratorBinding = adfTableBinding.getDCIteratorBinding();
        Object selectedRowData = table.getSelectedRowData();
        JUCtrlHierNodeBinding nodeBinding = (JUCtrlHierNodeBinding) selectedRowData;
        Key rwKey = nodeBinding.getRowKey();
        tableIteratorBinding.setCurrentRowWithKey(rwKey.toStringFormat(true));
        
    }
    
    /*
     *  mark the row selected when selected users table clicked
     */
    public void onSelectedTableSelect(SelectionEvent selectionEvent) {
        
        logger.finest("onSelectedTableSelect");
        RichTable table = (RichTable) selectionEvent.getSource();
        ArrayList<RequestedUser> tableModel = (ArrayList<RequestedUser>)table.getValue();
        //UCtrlHierBinding adfTableBinding = (JUCtrlHierBinding) tableModel.getWrappedData();
        //DCIteratorBinding tableIteratorBinding = adfTableBinding.getDCIteratorBinding();
        
        //Object selectedRowData = table.getSelectedRowData();
        //JUCtrlHierNodeBinding nodeBinding = (JUCtrlHierNodeBinding) selectedRowData;
        //Key rwKey = nodeBinding.getRowKey();
        RowKeySet rowKeySet = table.getSelectedRowKeys();
        logger.finest("Rowkeyset:" + rowKeySet);
        
        rowKeySet.clear();
        //SelectionEvent selectEvent = new SelectionEvent(table.getSelectedRowKeys(),rowKeySet,table);
        //selectEvent.queue();
        //tableIteratorBinding.setCurrentRowWithKey(rwKey.toStringFormat(true));
        
    }
    
    private boolean validateCriteria(UserSearchVOCriteria parmMap) {
        
        String displayName = (String) parmMap.getDisplayName();
        if (displayName != null && displayName.trim().isEmpty())
            displayName = null;
        String login = (String) parmMap.getUserLogin();
        if (login != null && login.trim().isEmpty())
            login = null;
        String email = (String) parmMap.getEmail();
        if (email != null && email.trim().isEmpty())
            email = null;
        String roleName = (String) parmMap.getRoleKey();
        if (roleName != null && roleName.trim().isEmpty())
            roleName = null;
        String orgName = (String) parmMap.getOrgKey();
        if (orgName != null && orgName.trim().isEmpty())
            orgName = null;

        if (displayName == null && login == null && roleName == null && orgName == null && email == null) {
            FacesMessage fm = new FacesMessage("Please specify at least one search value");
            fm.setSeverity(FacesMessage.SEVERITY_WARN);
            FacesContext fctx = FacesContext.getCurrentInstance();
            fctx.addMessage(null, fm);
            return false;
        }
        return true;

        
    }

    public void setDisplayNameBind(RichInputText displayNameBind) {
        this.displayNameBind = displayNameBind;
    }

    public RichInputText getDisplayNameBind() {
        return displayNameBind;
    }

    public void setUserLoginBind(RichInputText userLoginBind) {
        this.userLoginBind = userLoginBind;
    }

    public RichInputText getUserLoginBind() {
        return userLoginBind;
    }

    public void setEmailBinding(RichInputText emailBinding) {
        this.emailBinding = emailBinding;
    }

    public RichInputText getEmailBinding() {
        return emailBinding;
    }

    public void setRoleKeyBinding(RichSelectOneChoice roleKeyBinding) {
        this.roleKeyBinding = roleKeyBinding;
       
    }

    public RichSelectOneChoice getRoleKeyBinding() {
        return roleKeyBinding;
    }

    public void setOrgKeyBinding(RichSelectOneChoice orgKeyBinding) {
        this.orgKeyBinding = orgKeyBinding;
    }

    public RichSelectOneChoice getOrgKeyBinding() {
        return orgKeyBinding;
    }


    public void setRoleKey(String roleKey) {
        logger.finest("setRoleKey");
        this.roleKey = roleKey;
    }

    public String getRoleKey() {
        return roleKey;
    }

    public void setOrgKey(String orgKey) {
        this.orgKey = orgKey;
    }

    public String getOrgKey() {
        return orgKey;
    }
    
    public void setSelectedUser(String selectedUser) {
            this.selectedUser = selectedUser;
        }

    public String getSelectedUser() {
        return selectedUser;
    }


    public void setSelectedUserRow(FacesCtrlHierNodeBinding selectedUserRow) {
        this.selectedUserRow = selectedUserRow;
    }

    public FacesCtrlHierNodeBinding getSelectedUserREow() {
        return selectedUserRow;
    }


    public void setRequestedUsers(List<RequestedUser> requestedUsers) {
        this.requestedUsers = requestedUsers;
    }

    public List<RequestedUser> getRequestedUsers() {
        return requestedUsers;
    }


    public void setDeletedUserRow(RequestedUser deletedUserRow) {
        this.deletedUserRow = deletedUserRow;
    }

    public RequestedUser getDeletedUserRow() {
        return deletedUserRow;
    }

}
