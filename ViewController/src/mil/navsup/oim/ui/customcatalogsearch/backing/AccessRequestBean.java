package mil.navsup.oim.ui.customcatalogsearch.backing;

import java.math.BigInteger;

import java.util.ArrayList;
import java.util.List;

import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.event.ActionEvent;

import mil.navsup.oim.ui.customcatalogsearch.model.eo.RequestedUser;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.data.RichTable;

import mil.navsup.oim.ui.customcatalogsearch.utils.NavyFacesUtils;

import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.model.BindingContext;
import oracle.jbo.Row;
import java.io.Serializable;

import java.util.HashMap;

import mil.navsup.oim.ui.customcatalogsearch.model.eo.RequestedCatalogItem;
import mil.navsup.oim.ui.customcatalogsearch.utils.NavyADFOIMUtils;
import mil.navsup.oim.ui.customcatalogsearch.utils.NavyADFUtils;

import oracle.iam.ui.platform.utils.FacesUtils;


public class AccessRequestBean implements Serializable {
    
    private RichTable availableUsers;
    private RichTable availableItems;
    private RichTable selectedUsersTable;
    private RichTable selectedItemsTable;
    //private String selectedUser;
    //private String selectedItem;
    private List<RequestedUser> selectedUsers = new ArrayList<RequestedUser>();
    private List<RequestedUser> availableUsersList = new ArrayList<RequestedUser>();
    private List<RequestedCatalogItem> selectedItems = new ArrayList<RequestedCatalogItem>();
    private String selectedNDPRole;
    private String requestMessage;
    
    
    private ADFLogger logger = ADFLogger.createADFLogger(AccessRequestBean.class);


    public AccessRequestBean() {
    }

    public void setAvailableUsers(RichTable availableUsers) {
        this.availableUsers = availableUsers;
    }

    public RichTable getAvailableUsers() {
        return availableUsers;
    }

    public boolean isUserSelected() {
        logger.finest("isUserSelected");
        String strKey = (String) NavyFacesUtils.getValueFromELExpression("#{row.usrKey}");
        if (strKey == null) {
            logger.finest("usrKey not found!");
            return false;
        }
        
        logger.finest("Key:" + strKey);
        logger.finest("num selected:" + this.selectedUsers.size());
        for (RequestedUser u : this.selectedUsers) {
            logger.finest("Compare Table key:" + u + " to selected key:" + strKey);
            if (strKey.equals(u.getUsrKey())) {
                logger.finest("User is selected:" + strKey);
                return true;
            }
        }

        return false;
    }
    
    public boolean isItemSelected() {
        logger.finest("isItemSelected");
        String strKey = (String) NavyFacesUtils.getValueFromELExpression("#{row.id}");
        if (strKey == null) {
            logger.finest("Item ket not found!");
            return false;
        }
        
        logger.finest("Key:" + strKey);
        logger.finest("num selected:" + this.selectedItems.size());
        for (RequestedCatalogItem item : this.selectedItems) {
            logger.finest("Compare Table key:" + item + " to selected key:" + strKey);
            if (strKey.equals(item.getId())) {
                logger.finest("Item is selected:" + strKey);
                return true;
            }
        }

        return false;
    }
    
    

    public void setSelectedUsers(List<RequestedUser> selectedUsers) {
        this.selectedUsers = selectedUsers;
    }

    public List<RequestedUser> getSelectedUsers() {
        if (this.selectedUsers == null) {
            this.selectedUsers = new ArrayList<>();
        }
        return this.selectedUsers;
    }

    public void addUser(String userId) {

        logger.finest("addUser:" + userId);
        RequestedUser user = new RequestedUser();
        user.setUsrId(userId);
        user.setUsrKey(userId);
        if (this.selectedUsers == null) {
            this.selectedUsers = new ArrayList<RequestedUser>();
            this.selectedUsers.add(user);
        } else {
            this.selectedUsers.add(user);
        }
    }


    public void addUserById(RequestedUser user) {
        logger.finest("addUserById");
        logger.finest("got user addUserById:" + user);
  
        if (this.selectedUsers == null) {
            this.selectedUsers = new ArrayList<RequestedUser>();
            this.selectedUsers.add(user);
        } else {
            this.selectedUsers.add(user);
        }
        logger.finest("SelectedUsers:" + this.selectedUsers);
    }


    public boolean removeUserById(RequestedUser user) {
        logger.finest("removeUserById");
        logger.finest("got user removeUserById:" + user.getUsrKey());
        if (this.selectedUsers == null) {
            return false;
        }

        //RequestedUser user = findCurrentUserById(userId);
        if (user != null) {
            this.selectedUsers.remove(user);
        }


        NavyADFUtils.partialRender(getSelectedUsersTable());
        NavyADFUtils.partialRender(getAvailableUsers());
        //updateCountUI();

        return true;
    }

    private RequestedUser findCurrentUserById(String userId) {
        RequestedUser user = null;

        for (RequestedUser u : this.selectedUsers) {
            if (null != u && u.getUsrKey().equals(userId)) {
                user = u;
                break;
            }
        }
        return user;
    }
    
    
    public void addItemById(RequestedCatalogItem item) {
        logger.finest("addItemById");
        logger.finest("got item addItemById:" + item);
    
        if (this.selectedItems == null) {
            this.selectedItems = new ArrayList<RequestedCatalogItem>();
            this.selectedItems.add(item);
        } else {
            this.selectedItems.add(item);
        }
        logger.finest("SelectedItems:" + this.selectedItems);
    }
    
    
    public boolean removeItemById(RequestedCatalogItem item) {
        logger.finest("removeItemById");
        logger.finest("got item removeItemById:" + item.getId());
        if (this.selectedItems == null) {
            return false;
        }

        //RequestedUser user = findCurrentUserById(userId);
        if (item != null) {
            this.selectedItems.remove(item);
        }


        NavyADFUtils.partialRender(getSelectedItemsTable());
        NavyADFUtils.partialRender(getAvailableItems());
        //updateCountUI();

        return true;
    }
    
    public void showUserInformationDialog(ActionEvent actionEvent) {
        String title = FacesUtils.getBundleValue("oracle.iam.ui.OIMUIBundle", "GENERIC_USER_DETAILS");
        String userInfoTaskFlow = "/WEB-INF/oracle/iam/ui/common/tfs/user-information-tf.xml#user-information-tf";

        String id = (String)FacesUtils.getValueFromELExpression("#{row.entityId}");
        if (id == null) {
          id = (String)FacesUtils.getValueFromELExpression("#{row.usrKey}");
        }
          if (id == null) {
            id = (String)FacesUtils.getValueFromELExpression("#{row.id}");
        }
        String name = (String)FacesUtils.getValueFromELExpression("#{row.displayName}");

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("userId", id);
        params.put("userName", name);
        logger.finest("Call user info taskflow with parms:" + params);
        NavyADFOIMUtils.raiseTaskFlowLaunchEvent("user_information_dialog", userInfoTaskFlow, title, null, null, null, true, params);
      }
    
    /*

    public void setSelectedUser(String selectedUser) {
        this.selectedUser = selectedUser;
    }

    public String getSelectedUser() {
        return selectedUser;
    }
*/

    public void setSelectedUsersTable(RichTable selectedUsersTable) {
        this.selectedUsersTable = selectedUsersTable;
    }

    public RichTable getSelectedUsersTable() {
        return selectedUsersTable;
    }


    public void setSelectedItemsTable(RichTable selectedItemsTable) {
        this.selectedItemsTable = selectedItemsTable;
    }

    public RichTable getSelectedItemsTable() {
        return selectedItemsTable;
    }


    public void setAvailableItems(RichTable availableItems) {
        this.availableItems = availableItems;
    }

    public RichTable getAvailableItems() {
        return availableItems;
    }

/*
    public void setSelectedItem(String selectedItem) {
        
        this.selectedItem = selectedItem;
    }

    public String getSelectedItem() {
        return selectedItem;
    }
*/
    public void setSelectedItems(List<RequestedCatalogItem> selectedItems) {
        this.selectedItems = selectedItems;
    }

    public List<RequestedCatalogItem> getSelectedItems() {
        return selectedItems;
    }


    public void setAvailableUsersList(List<RequestedUser> availableUsersList) {
        logger.finest("setAvailableUsersList");
        if (availableUsersList != null)
            logger.finest("setAvailableUsersList:" + availableUsersList.size());
        this.availableUsersList = availableUsersList;
    }

    public List<RequestedUser> getAvailableUsersList() {
        logger.finest("getAvailableUsersList");
        return availableUsersList;
    }


    public void setSelectedNDPRole(String selectedNDPRole) {
        this.selectedNDPRole = selectedNDPRole;
    }

    public String getSelectedNDPRole() {
        return selectedNDPRole;
    }


    public void setRequestMessage(String requestMessage) {
        this.requestMessage = requestMessage;
    }

    public String getRequestMessage() {
        return requestMessage;
    }

}
