package mil.navsup.oim.ui.customcatalogsearch.backing;

import java.io.Serializable;

import java.util.List;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import mil.navsup.oim.ui.customcatalogsearch.model.eo.RequestedCatalogItem;
import mil.navsup.oim.ui.customcatalogsearch.model.eo.RequestedUser;
import mil.navsup.oim.ui.customcatalogsearch.request.GenerateRequestUtilities;
import mil.navsup.oim.ui.customcatalogsearch.utils.NavyADFUtils;

import oracle.adf.controller.ControllerContext;
import oracle.adf.controller.TaskFlowContext;
import oracle.adf.controller.TaskFlowTrainModel;
import oracle.adf.controller.TaskFlowTrainStopModel;
import oracle.adf.controller.ViewPortContext;
import oracle.adf.view.rich.component.rich.RichPopup;

@SuppressWarnings("oracle.jdeveloper.java.serialversionuid-field-missing")
public class AccessRequestNavigationHandler implements Serializable {

    private static Logger logger = Logger.getLogger(AccessRequestNavigationHandler.class.getName());

    boolean firstStop = false;
    boolean lastStop = false;
    private RichPopup backButtonWarning;
    private RichPopup checkoutPageTrainStopWarning;
    boolean isUserWantToGoBack = true;
    boolean isUserWantToGoOtherStop = true;
    private TrainHandlerBeanHelper trainHandlerBeanHelper = null;
  

    public AccessRequestNavigationHandler() {
        super();
    }

    public String navigateNextStop() {
        
        logger.finest("navigateNextStop");
        TaskFlowTrainModel tm = getTrainModel();

        TaskFlowTrainStopModel currentStop = tm.getCurrentStop();
        TaskFlowTrainStopModel nextStop = tm.getNextStop(currentStop);

        if (isLastStop()) {
            logger.finest("last stop can't go forward anymore; disable the button");
            return null;
        }
        
        if (!this.validate(currentStop.getLocalActivityId())) {
            String errorMsg = "Please make a selection";
            errorMsg = "<html><body>" + errorMsg + "</body></html>";
            FacesMessage fm = new FacesMessage(errorMsg.toString());
            fm.setSeverity(FacesMessage.SEVERITY_WARN);
            FacesContext fctx = FacesContext.getCurrentInstance();
            fctx.addMessage(null, fm);
            return null;
        }
        
        StringBuilder message = this.getCustomCatalogRequestValidator().validateRequest();
        if (message.length() > 0) {
            
            String errorMsg = message.toString();
            errorMsg = "<html><body>" + errorMsg + "</body></html>";
            FacesMessage fm = new FacesMessage(errorMsg.toString());
            fm.setSeverity(FacesMessage.SEVERITY_WARN);
            FacesContext fctx = FacesContext.getCurrentInstance();
            fctx.addMessage(null, fm);
            return null;
            
        }
        
        logger.finest("check for last stop");
        
        if (nextStop.getLocalActivityId().equals("Done")) {
            logger.finest("do last stop");
            List<RequestedCatalogItem> selectedItems = this.getAccessRequestBean().getSelectedItems();
            List<RequestedUser> selectedUsers = this.getAccessRequestBean().getSelectedUsers();
            GenerateRequestUtilities reqUtils = new GenerateRequestUtilities();
            String entityTypes = this.getCustomCatalogRequestValidator().getCommonRequestedEntityType();
            logger.finest("call generate request with:" + entityTypes);
            message = reqUtils.generateRequest(selectedItems, selectedUsers, entityTypes);
            if (message.toString().contains("Request created")) {
                this.getAccessRequestBean().setRequestMessage(message.toString());
            }
            if (message.toString().contains("Error")) {
                String errorMsg = message.toString();
                errorMsg = "<html><body>" + errorMsg + "</body></html>";
                FacesMessage fm = new FacesMessage(errorMsg.toString());
                fm.setSeverity(FacesMessage.SEVERITY_WARN);
                FacesContext fctx = FacesContext.getCurrentInstance();
                fctx.addMessage(null, fm);
                return null;
            }
        }
        
        logger.finest("current act id:" + currentStop.getLocalActivityId());
        logger.finest("current outcome id:" + currentStop.getOutcome());
        logger.finest("next act id:" + nextStop.getLocalActivityId());
        logger.finest("next Outcome:" + nextStop.getOutcome());
        return nextStop.getOutcome();
    }


    public String navigatePreviousStop() {
        logger.finest("navigatePreviousStop");
        if (this.isUserWantToGoBack) {

            TaskFlowTrainModel tm = getTrainModel();
            TaskFlowTrainStopModel prevStop = tm.getPreviousStop(tm.getCurrentStop());

            if (isFirstStop()) {
                logger.finest("first stop, can't go back anymore; disable the button");
                return null;
            }
            logger.finest("Outcome:" + prevStop.getOutcome());
            return prevStop.getOutcome();
        }

        return null;
    }


    public String navigateToTrainStop() {
        logger.finest("navigateToTrainStop");
        String trainStopOutcome = null;
        if (this.isUserWantToGoOtherStop) {
            trainStopOutcome = this.trainHandlerBeanHelper.getSelectedTrainStopOutcome();
        }
        logger.finest("Outcome:" + trainStopOutcome);
        return trainStopOutcome;
    }


    public void setFirstStop(boolean firstStop) {
        logger.finest("setFirstStop");
        this.firstStop = firstStop;
    }


    public boolean isFirstStop() {
        logger.finest("isFirstStop");
        TaskFlowTrainModel tm = getTrainModel();
        TaskFlowTrainStopModel prevStop = tm.getPreviousStop(tm.getCurrentStop());

        if (prevStop == null) {
            return true;
        }
        return false;
    }


    public void setLastStop(boolean lastStop) {
        logger.finest("setLastStop");
        this.lastStop = lastStop;
    }


    public boolean isLastStop() {
        logger.finest("isLastStop");
        TaskFlowTrainModel tm = getTrainModel();
        TaskFlowTrainStopModel nextStop = tm.getNextStop(tm.getCurrentStop());

        if (nextStop == null) {
            return true;
        }
        return false;
    }

    public String cancelAccessRequest() {
        logger.finest("cancelAccessRequest");
        return "cancel";
    }
    
    public boolean validate(String actId) {
        
        AccessRequestBean accessRequestBean = this.getAccessRequestBean();
        
        if (actId.equals("Users")) {
            if (accessRequestBean.getSelectedUsers() == null) {
                return false;
            }
            int numselectedUsers = this.getAccessRequestBean().getSelectedUsers().size();
            if (numselectedUsers == 0) {
                return false;
            }
        }
        
        if (actId.equals("Items")) {
            if (accessRequestBean.getSelectedItems() == null) {
                return false;
            }
            int numselectedUsers = this.getAccessRequestBean().getSelectedItems().size();
            if (numselectedUsers == 0) {
                return false;
            }
        }
        
       
        return true;
        
        
    }
    
    private CatalogRequestValidator getCustomCatalogRequestValidator() {
       
        return (CatalogRequestValidator) NavyADFUtils.getPageFlowScopeParameter("CustomCatalogRequestValidatorBean");
     }

    
    private AccessRequestBean getAccessRequestBean() {
       
        return (AccessRequestBean) NavyADFUtils.getPageFlowScopeParameter("AccessRequestBean");
     }


    private TaskFlowTrainModel getTrainModel() {
        ControllerContext controllerContext = ControllerContext.getInstance();
        ViewPortContext currentViewPortCtx = controllerContext.getCurrentViewPort();
        TaskFlowContext taskFlowCtx = currentViewPortCtx.getTaskFlowContext();
        return taskFlowCtx.getTaskFlowTrainModel();
    }

    public void setTrainHandlerBeanHelper(TrainHandlerBeanHelper trainHandlerBeanHelper) {
        this.trainHandlerBeanHelper = trainHandlerBeanHelper;
    }


    public TrainHandlerBeanHelper getTrainHandlerBeanHelper() {
        return this.trainHandlerBeanHelper;
    }
}
