package mil.navsup.oim.ui.customcatalogsearch.request;

import java.util.ArrayList;
import java.util.List;

import mil.navsup.oim.ui.customcatalogsearch.model.eo.RequestedCatalogItem;
import mil.navsup.oim.ui.customcatalogsearch.model.eo.RequestedUser;
import mil.navsup.oim.ui.customcatalogsearch.utils.NavyADFOIMUtils;

import oracle.adf.share.logging.ADFLogger;

import oracle.iam.api.OIMService;
import oracle.iam.platform.utils.vo.OIMType;
import oracle.iam.request.api.RequestService;
import oracle.iam.request.vo.Beneficiary;
import oracle.iam.request.vo.RequestBeneficiaryEntity;
import oracle.iam.request.vo.RequestConstants;
import oracle.iam.request.vo.RequestData;
import oracle.iam.vo.OperationResult;

public class GenerateRequestUtilities {


    private ADFLogger logger = ADFLogger.createADFLogger(GenerateRequestUtilities.class);
    private final OIMService oimService;
    private final NavyADFOIMUtils oimUtils;
    
    public GenerateRequestUtilities() {
        super();
        this.oimUtils = new NavyADFOIMUtils();
        this.oimService = oimUtils.getOIMService();
        
    }

    public StringBuilder generateRequest(List<RequestedCatalogItem> selectedItems,List<RequestedUser> selectedUsers,String entityTypes) {
        StringBuilder message = new StringBuilder();
        logger.finest("generateRequest:" + entityTypes);
        //logger.finest("generateRequest:" + OIMType.Entitlement.getValue());
        OperationResult result = null;
        if (entityTypes.equals(OIMType.Entitlement.getValue())) {
            try {
                result = this.requestToProvisionEntitlement(selectedUsers, selectedItems);
                logger.finest("OperationResult:" + result);
                String reqId = result.getRequestID();
                if (reqId != null) {
                    message.append("Entitlement Request created with request ID:" + reqId);
                }
            } catch (Exception e) {
                message.append("Error creating Entitlement request:" + e.getMessage());
                logger.severe("Error creating Entitlement request:" + e.getMessage(),e);
                
            }
        }
        else
        if (entityTypes.equals(OIMType.Role.getValue())) {
           
            try {
                result = this.requestToProvisionRole(selectedUsers, selectedItems);
                logger.finest("OperationResult:" + result);
                String reqId = result.getRequestID();
                if (reqId != null) {
                    message.append("Request created with request ID:" + reqId);
                }
            } catch (Exception e) {
                message.append("Error creating Role request:" + e.getMessage());
                logger.severe("Error creating Role request:" + e.getMessage(),e);
                
            }
        }
        else
        if (entityTypes.equals(OIMType.ApplicationInstance.getValue())) {
            try {
                result = this.requestToProvisionApplication(selectedUsers, selectedItems);
                logger.finest("OperationResult:" + result);
                String reqId = result.getRequestID();
                if (reqId != null) {
                    message.append("Request created with request ID:" + reqId);
                }
            } catch (Exception e) {
                message.append("Error creating Application request:" + e.getMessage());
                logger.severe("Error creating Application request:" + e.getMessage(),e);
                
            }
        }
        return message;
    }
    
    public OperationResult requestToProvisionEntitlement(List<RequestedUser> selectedUsers, List<RequestedCatalogItem> selectedItems) throws Exception {
             
        List<RequestBeneficiaryEntity> entities = new ArrayList<RequestBeneficiaryEntity>();
        for(RequestedCatalogItem selectedItem : selectedItems)
        {
            RequestBeneficiaryEntity reqBenefEntity = new RequestBeneficiaryEntity();
            reqBenefEntity.setRequestEntityType(OIMType.Entitlement);
            reqBenefEntity.setEntitySubType(selectedItem.getEntityName());
            reqBenefEntity.setEntityKey(selectedItem.getEntityKey());
            reqBenefEntity.setOperation(RequestConstants.MODEL_PROVISION_ENTITLEMENT_OPERATION);
            entities.add(reqBenefEntity);
        }
        List<Beneficiary> beneficiaries = new ArrayList<Beneficiary>();
        for(RequestedUser selectedUser : selectedUsers )
        {
            Beneficiary beneficiary = new Beneficiary();
            beneficiary.setBeneficiaryKey(selectedUser.getUsrKey());
            beneficiary.setBeneficiaryType(Beneficiary.USER_BENEFICIARY);
            beneficiary.setTargetEntities(entities);
            beneficiaries.add(beneficiary);
        }

        // Setup Request Data
        RequestData reqData = new RequestData();
        reqData.setBeneficiaries(beneficiaries); // Set list of request entity

        // Invoke request operation in OIM
        RequestService reqOp = oimUtils.getRequestOp();
        String reqId = reqOp.submitRequest(reqData);
        //OperationResult res = new OperationResult();
        //res.setOperationStatus(OperationResult.OperationStatus.SUCCESSFUL);
        //res.setRequestID(reqId);
        OperationResult result = oimService.doOperation(reqData, OIMService.Intent.REQUEST);

        return result;
    }
    
    public OperationResult requestToProvisionRole(List<RequestedUser> selectedUsers, List<RequestedCatalogItem> selectedItems) throws Exception {
        List<RequestBeneficiaryEntity> entities = new ArrayList<RequestBeneficiaryEntity>();
        for(RequestedCatalogItem selectedItem : selectedItems)
        {
            RequestBeneficiaryEntity reqBenefEntity = new RequestBeneficiaryEntity();
            reqBenefEntity.setRequestEntityType(OIMType.Role);
            reqBenefEntity.setEntitySubType(selectedItem.getEntityName());
            reqBenefEntity.setEntityKey(selectedItem.getEntityKey());
            reqBenefEntity.setOperation(RequestConstants.MODEL_ROLE_GRANT_OPERATION);
            entities.add(reqBenefEntity);
        }
        List<Beneficiary> beneficiaries = new ArrayList<Beneficiary>();
        for(RequestedUser selectedUser : selectedUsers )
        {
            Beneficiary beneficiary = new Beneficiary();
            beneficiary.setBeneficiaryKey(selectedUser.getUsrKey());
            beneficiary.setBeneficiaryType(Beneficiary.USER_BENEFICIARY);
            beneficiary.setTargetEntities(entities);
            beneficiaries.add(beneficiary);
        }

        // Setup Request Data
        RequestData reqData = new RequestData();
        reqData.setBeneficiaries(beneficiaries); // Set list of request entity

        // Invoke request operation in OIM
        
        OperationResult result = oimService.doOperation(reqData, OIMService.Intent.REQUEST);

        return result;
    }
    
    public OperationResult requestToProvisionApplication(List<RequestedUser> selectedUsers, List<RequestedCatalogItem> selectedItems) throws Exception {
        List<RequestBeneficiaryEntity> entities = new ArrayList<RequestBeneficiaryEntity>();
        for(RequestedCatalogItem selectedItem : selectedItems)
        {
            logger.finest("Create request for:" + selectedItem);
            RequestBeneficiaryEntity reqBenefEntity = new RequestBeneficiaryEntity();
            reqBenefEntity.setRequestEntityType(OIMType.ApplicationInstance);
            reqBenefEntity.setEntitySubType(selectedItem.getEntityName());
            reqBenefEntity.setEntityKey(selectedItem.getEntityKey());
            reqBenefEntity.setOperation(RequestConstants.MODEL_PROVISION_APPLICATION_INSTANCE_OPERATION);
            entities.add(reqBenefEntity);
        }
        List<Beneficiary> beneficiaries = new ArrayList<Beneficiary>();
        for(RequestedUser selectedUser : selectedUsers )
        {
            Beneficiary beneficiary = new Beneficiary();
            beneficiary.setBeneficiaryKey(selectedUser.getUsrKey());
            beneficiary.setBeneficiaryType(Beneficiary.USER_BENEFICIARY);
            beneficiary.setTargetEntities(entities);
            beneficiaries.add(beneficiary);
        }

        // Setup Request Data
        RequestData reqData = new RequestData();
        reqData.setBeneficiaries(beneficiaries); // Set list of request entity

        // Invoke request operation in OIM
        OperationResult result = oimService.doOperation(reqData, OIMService.Intent.REQUEST);

        return result;
    }
    
}
